package com.danareksa.investasik.ui.fragments.redemption;

import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.RedemptionFee;
import com.danareksa.investasik.data.api.responses.PartialRedemtionResponse;
import com.danareksa.investasik.data.api.responses.RedeemFeeResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by asep.surahman on 20/08/2018.
 */

public class ConfirmationNewPresenter {

    private ConfirmationNewFragment fragment;
    private String sysDate, minimalDate;
    private boolean statusCheckDate = false;

    public ConfirmationNewPresenter(ConfirmationNewFragment fragment) {
        this.fragment = fragment;
    }



    void redemptionFee(final Packages packages) {
        fragment.showProgressBar();
        fragment.getApi().redemptionFee(packages.getId(), 2l, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<RedemptionFee>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(List<RedemptionFee> list) {
                        fragment.dismissProgressBar();

                        if (list.size() > 0) {
                            fragment.redemptionFeeSetups = list;
                            fragment.bindInvestmentData();
                        }
                    }
                });

    }

    void getRedemptionFee(String investmentAccountNo) {
        fragment.getApi().redemptionFee(investmentAccountNo, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<RedeemFeeResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(RedeemFeeResponse redeemFeeResponse) {

                        fragment.dismissProgressBar();

                        if(redeemFeeResponse.getCode() == 1 && redeemFeeResponse.getInfo().equalsIgnoreCase("data found")){
                            fragment.redemFee = redeemFeeResponse.getData().getRedeemFee();
                            fragment.redemptionFees = redeemFeeResponse;
                        }else if(redeemFeeResponse.getCode() == 0 && redeemFeeResponse.getInfo().equalsIgnoreCase("data not found")){
                            fragment.redemFee = 0.0;
                        }else{
                            fragment.redemFee = 0.0;
                        }

                        fragment.bindInvestmentData();
                        fragment.initUnit();
                        fragment.initInvest();
                        fragment.cbAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                                        @Override
                                                                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                                            if (isChecked) {
                                                                                fragment.bConfirm.setEnabled(true);
                                                                            } else {
                                                                                fragment.bConfirm.setEnabled(false);
                                                                            }
                                                                        }
                                                                    }
                        );


                    }
                });

    }

    void getRangeOfPartialRedemtion(String investmentAccountNo) {
        fragment.showProgressBar();
        fragment.getApi().rangeOfPartialRedemtion(investmentAccountNo, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PartialRedemtionResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(PartialRedemtionResponse partialRedemtionRsp) {
                        if (partialRedemtionRsp.getCode() == 1){
                            fragment.partialRedemtionResponse = partialRedemtionRsp;
                            Double min = partialRedemtionRsp.getData().getMinPartialRedemption() * 100;
                            Double max = partialRedemtionRsp.getData().getMaxPartialRedemption() * 100;

                            checkStatusMinimalRedeem(partialRedemtionRsp.getData().getMinimalInvestmentRedeemDate());
                            Log.d("status", "onNext: " + statusCheckDate);

                            if (partialRedemtionRsp.getData().getMinimalInvestmentAmount() <= 0 && !statusCheckDate) {
                                if (min >= max) {
                                    fragment.llvalidasi.setVisibility(View.VISIBLE);
                                    fragment.toggleButtonRedeemtion.setEnabled(false);
                                    fragment.answer = "full";
                                } else {
                                    fragment.min = min.intValue();
                                    fragment.max = max.intValue();
                                }
                            } else {
                                fragment.min = min.intValue();
                                fragment.max = max.intValue();

                                //fragment.etAmount.setEnabled(true);
                                fragment.tvValidasi.setText("Investasi ini hanya dapat dijual sebagian, penjualan secara keseluruhan hanya dapat dilakukan setelah " + minimalDate);
                                fragment.llvalidasi.setVisibility(View.VISIBLE);
                                fragment.toggleButtonRedeemtion.setChecked(false);
                                fragment.toggleButtonRedeemtion.setEnabled(false);
                            }

                            getRedemptionFee(fragment.investment.getInvestmentAccountNumber());

                        } else {
                            Toast.makeText(fragment.getContext(), partialRedemtionRsp.getInfo(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    void checkStatusMinimalRedeem(String minimalRedeemDate) {
        SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault());
        Date localDate = new Date();
        sysDate = dtf.format(localDate);

        if (minimalRedeemDate == null) {
            minimalRedeemDate = sysDate;
        }
        String string = minimalRedeemDate;
        String[] parts = string.split("-");
        String tanggal = parts[2].substring(0, 2);
        int day = Integer.parseInt(tanggal);
        int month = Integer.parseInt(parts[1]);
        int year = Integer.parseInt(parts[0]);

        String stringCurrent = sysDate;
        String[] partsCurrent = stringCurrent.split("-");
        String tanggalCurrent = partsCurrent[2].substring(0, 2);
        int dayCurrent = Integer.parseInt(tanggalCurrent);
        int monthCurrent = Integer.parseInt(partsCurrent[1]);
        int yearCurrent = Integer.parseInt(partsCurrent[0]);

        if (year > yearCurrent) {
            statusCheckDate = true;
        } else {
            if (month > monthCurrent) {
                statusCheckDate = true;
            } else {
                statusCheckDate = day > dayCurrent;
            }
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        SimpleDateFormat formatDate = new SimpleDateFormat("dd MMMM yyyy");
        minimalDate = formatDate.format(calendar.getTime());
        Log.d("tgl", "checkStatusMinimalRedeem: " + minimalDate + " " + minimalRedeemDate);
    }

}
