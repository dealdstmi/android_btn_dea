package com.danareksa.investasik.ui.fragments.checkout;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.BcaKlikPayData;
import com.danareksa.investasik.data.api.beans.CartList;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.CheckoutActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.ui.fragments.termsandcondition.TermsAndConditionFragment;

import java.io.IOException;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import timber.log.Timber;

/**
 * Created by asep.surahman on 13/08/2018.
 */

public class BcaKlikpayFragment extends BaseFragment{

    public static final String TAG = TermsAndConditionFragment.class.getSimpleName();
    public static Activity activity;
    private final static String URL_CANCEL_1 = "https://simpg.sprintasia.net/klikpay/simulator/logout.php?noredirect";
    private final static String URL_CANCEL_2 = "https://simpg.sprintasia.net/klikpay/simulator/logout?noredirect";
    private final static String URL_BATAL_1 = "https://simpg.sprintasia.net/klikpay/simulator/logout.php?noredirect";

    @Bind(R.id.webView)
    WebView webView;

    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    static String webUrl;
    public static final String BCA_KLIK_PAY = "bcaKlikPayTrans";
    private final static String CART_LIST = "cartList";
    String url;
    String referrer;
    String paymentType;
    String transNo;
    BcaKlikPayData bcaKlikpayTrans;
    @State
    public CartList cartList;
    BcaKlikpayPresenter presenter;

    public static void showFragment(BaseActivity sourceActivity, CartList cartList, String webUrl, String referrer, BcaKlikPayData bcaKlikPayData, String paymentType, String transNo) {

        if (!sourceActivity.isFragmentNotNull(TAG)) {
            activity = sourceActivity;
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            BcaKlikpayFragment fragment = new BcaKlikpayFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(BCA_KLIK_PAY, bcaKlikPayData);
            bundle.putString("url", webUrl);
            bundle.putString("referrer", referrer);
            bundle.putString("paymentType", paymentType);
            bundle.putString("transNo", transNo);
            bundle.putSerializable(CART_LIST, cartList);
            fragment.setArguments(bundle);
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }


    public static void showFragment(BaseActivity sourceActivity, int code) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            activity = sourceActivity;
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.container, new BcaKlikpayFragment(), TAG);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        presenter = new BcaKlikpayPresenter(this);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cartList = (CartList) getArguments().getSerializable(CART_LIST);
        url = getArguments().getString("url");
        referrer = getArguments().getString("referrer");
        paymentType = getArguments().getString("paymentType");
        transNo = getArguments().getString("transNo");
        bcaKlikpayTrans = (BcaKlikPayData) getArguments().getSerializable(BCA_KLIK_PAY);
        postURL(url);
    }


    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    @Override
    protected int getLayout() {
        return R.layout.f_payment_webview;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick(R.id.tvTryAgain)
    void retryConnection(){

    }


    protected void postURL(final String url){

        //final WebView wv = (WebView) activity.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        //webView.getSettings().setDomStorageEnabled(true);

        String klikPayCode     = bcaKlikpayTrans.getData().getKlikPayCode();
        String totalAmount     = bcaKlikpayTrans.getData().getTotalAmount();
        String payType         = bcaKlikpayTrans.getData().getPayType();
        String signature       = bcaKlikpayTrans.getData().getSignature();
        String transactionNo   = bcaKlikpayTrans.getData().getTransactionNo();
        String callback        = bcaKlikpayTrans.getData().getCallback();
        String currency        = bcaKlikpayTrans.getData().getCurrency();
        String transactionDate = bcaKlikpayTrans.getData().getTransactionDate();
        String miscFee         = bcaKlikpayTrans.getData().getMiscFee();
        String descp           = bcaKlikpayTrans.getData().getDescp();

        RequestBody formBody = new FormBody.Builder()
                .addEncoded("klikPayCode", klikPayCode)
                .addEncoded("totalAmount", totalAmount)
                .addEncoded("payType", payType)
                .addEncoded("signature", signature)
                .addEncoded("transactionNo", transactionNo)
                .addEncoded("callback", callback)
                .addEncoded("currency", currency)
                .addEncoded("transactionDate", transactionDate)
                .addEncoded("miscFee", miscFee)
                .addEncoded("descp", descp)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Referer", referrer)
                .addHeader("Cache-Control", "no-cache")
                .post(formBody)
                .build();


        new OkHttpClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e){
                Timber.e("failur : " + e.getMessage());
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String htmlString = response.body().string();
                webView.post(new Runnable(){
                    @Override
                    public void run() {
                        webView.clearCache(true);
                        webView.loadDataWithBaseURL(url, htmlString, "text/html", "utf-8", null);
                    }
                });
            }
        });



        webView.setWebViewClient(new WebViewClient(){
            public void onPageFinished(WebView view, String url){
                System.out.println("Url :: " + url);
                //http://13.251.61.30:8080/avantrade-portal-core/payment/BCAKlikPayPaymentFlag <- url callback
            }


            @Override
            public void onLoadResource(WebView view, String url){ //destination url
                super.onLoadResource(view, url);
                if(url.equals(bcaKlikpayTrans.getData().getCallback())){
                    presenter.getInfoPayment(bcaKlikpayTrans.getData().getTransactionNo());
                }else if(url.equals(URL_CANCEL_1) || url.equals(URL_CANCEL_2)){
                    presenter.getInfoPayment(bcaKlikpayTrans.getData().getTransactionNo());
                }
            }

        });
    }


    public void fetchResult(){
        SummaryBcaKlikpayFragment.showFragment((BaseActivity) getActivity(), cartList, bcaKlikpayTrans, paymentType);
        ((CheckoutActivity) getActivity()).setStep(3);
    }


    void showDialog(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which){
                        dialog.dismiss();
                        getActivity().finish();
                    }
                })
                .show();
    }

    public void handleBackpressed(){
        presenter.getInfoPayment(bcaKlikpayTrans.getData().getTransactionNo());
    }



    /*  if (webUrl.contains("true")) {
                    System.out.println("WEB_URL ======================= "+ webUrl );
                    String result[] = webUrl.split("=");
                    String on[] = result[1].split("&");
                    Intent intent = new Intent(getApplicationContext(), CCResultActivity.class);
                    intent.putExtra("key", "Your Payment is Successful \n Your Order Number = " + on[0]);
                    startActivity(intent);
                    finish();
                } else if (webUrl.contains("false")) {
                    String result[] = webUrl.split("=");
                    String on[] = result[1].split("&");
                    Intent intent = new Intent(getApplicationContext(), CCResultActivity.class);
                    intent.putExtra("key", "Your Payment is Failed \n Please Try Again");
                    startActivity(intent);
                    finish();
                }*/



}
