package com.danareksa.investasik.ui.fragments.tambahrekening;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.SelectInvestAccountActivity;
import com.danareksa.investasik.ui.adapters.pager.PilihInvestasiPagerAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.ui.NonSwipeableViewPager;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by asep.surahman on 25/07/2018.
 */

public class SelectInvestFragment extends BaseFragment {

    @Bind(R.id.pager)
    NonSwipeableViewPager pager;

    @Bind(R.id.bLumpsum)
    Button bLumpsum;

    @Bind(R.id.bReguler)
    Button bReguler;

    @Bind(R.id.bLanjut)
    Button bLanjut;

    private PilihInvestasiPagerAdapter pagerAdapter;

    @State
    int pageNumber = 0;

    String jenis_invest = "";

    int status = 0;

    public static final String TAG = SelectInvestFragment.class.getSimpleName();

    public static void showFragment(BaseActivity sourceActivity){
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new SelectInvestFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayout(){
        return R.layout.f_pilih_investasi;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        jenis_invest = Constant.INVEST_TYPE_LUMPSUM;
        status = 0;
        setupViewPager();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @OnClick(R.id.bLumpsum)
    void bLumpsum(){
        if(pageNumber > 0){
            pageNumber--;
            pager.setCurrentItem(pageNumber);
        }

        jenis_invest = Constant.INVEST_TYPE_LUMPSUM;
        if(status == 0){
            bLumpsum.setBackgroundResource(R.drawable.left_bg_round_white);
            bReguler.setBackgroundResource(R.drawable.right_bg_round_blue);
            bLumpsum.setTextColor(getResources().getColor(R.color.colorPrimary));
            bReguler.setTextColor(getResources().getColor(R.color.white));
            status = 1;
        }else if(status == 1){
            bLumpsum.setBackgroundResource(R.drawable.left_bg_round_blue);
            bReguler.setBackgroundResource(R.drawable.right_bg_round_white);
            bLumpsum.setTextColor(getResources().getColor(R.color.white));
            bReguler.setTextColor(getResources().getColor(R.color.colorPrimary));
            status = 0;
        }
    }


    @OnClick(R.id.bReguler)
    void bReguler(){
        if (pageNumber < pagerAdapter.getCount() - 1){
            pageNumber++;
            pager.setCurrentItem(pageNumber);
        }
        jenis_invest = Constant.INVEST_TYPE_REGULER;
        if(status == 0){
            bLumpsum.setBackgroundResource(R.drawable.left_bg_round_white);
            bReguler.setBackgroundResource(R.drawable.right_bg_round_blue);
            bLumpsum.setTextColor(getResources().getColor(R.color.colorPrimary));
            bReguler.setTextColor(getResources().getColor(R.color.white));
            status = 1;
        }else if(status == 1){
            bLumpsum.setBackgroundResource(R.drawable.left_bg_round_blue);
            bReguler.setBackgroundResource(R.drawable.right_bg_round_white);
            bLumpsum.setTextColor(getResources().getColor(R.color.white));
            bReguler.setTextColor(getResources().getColor(R.color.colorPrimary));
            status = 0;
        }
    }


    public void setupViewPager(){
        pagerAdapter = new PilihInvestasiPagerAdapter(this, getChildFragmentManager());
        pager.setAdapter(pagerAdapter);
        if(pageNumber == 0){
            bLanjut.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.bLanjut)
    void bLanjut(){
        SelectInvestAccountActivity.startActivity((BaseActivity) getActivity(), jenis_invest);
    }








}
