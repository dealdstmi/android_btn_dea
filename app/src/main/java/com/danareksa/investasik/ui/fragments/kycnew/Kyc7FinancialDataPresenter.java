package com.danareksa.investasik.ui.fragments.kycnew;

import com.danareksa.investasik.data.api.beans.Branch;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 18/07/2018.
 */

public class Kyc7FinancialDataPresenter {


    Kyc7FinancialDataFragment fragment;

    public Kyc7FinancialDataPresenter(Kyc7FinancialDataFragment fragment){
        this.fragment = fragment;
    }

    public void getBranch(final String bankId, final boolean status){
        fragment.showProgressBar();
        fragment.getApi().getListBranchByBankCodeNew(bankId, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Branch>>(){
                    @Override
                    public void onCompleted(){
                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                        fragment.isLoadingBranch = false;
                    }

                    @Override
                    public void onNext(List<Branch> branches){
                        fragment.dismissProgressBar();
                        fragment.branches = branches;
                        fragment.isLoadingBranch = true;
                        System.out.println("===> A1 ");
                        if(status == false){
                            fragment.setupSpinnerBranch(fragment.sBranchName, fragment.branches, "");
                        }else if(status == true){
                            fragment.fetchResultBranchToLayout();
                        }
                    }
                });
    }



}
