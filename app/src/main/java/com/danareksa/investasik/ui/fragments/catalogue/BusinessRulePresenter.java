package com.danareksa.investasik.ui.fragments.catalogue;

import com.danareksa.investasik.data.api.beans.Fee;
import com.danareksa.investasik.data.api.beans.FundAllocation;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.RedemptionFee;
import com.danareksa.investasik.data.api.responses.FundAllocationResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by fajarfatur on 2/2/16.
 */
public class BusinessRulePresenter {

    private BusinessRuleFragment fragment;

    public BusinessRulePresenter(BusinessRuleFragment fragment) {
        this.fragment = fragment;
    }

    void subscriptionFee(final Packages packages) {
        fragment.showSubscriptionLoading();
        fragment.getApi().subscriptionFee(packages.getId(), 1l, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Fee>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.hideSubscriptionLoading();
                    }

                    @Override
                    public void onNext(List<Fee> list) {
                        fragment.hideSubscriptionLoading();

                        if (list.size() > 0) {
                            fragment.subscriptionFees = list;
                            fragment.setupSubscriptionView();
                        }
                    }
                });

    }

    void redemptionFee(final Packages packages) {
        fragment.showRedemptionLoading();
        fragment.getApi().redemptionFee(packages.getId(), 2l, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<RedemptionFee>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.hideRedemptionLoading();
                    }

                    @Override
                    public void onNext(List<RedemptionFee> list) {
                        fragment.hideRedemptionLoading();

                        if (list.size() > 0) {
                            fragment.redemptionFees = list;
                            fragment.setupRedemptionView();
                        }
                    }
                });

    }

    void fundAllocation(Packages packages) {
        fragment.showRedemptionLoading();
        fragment.getApi().fundAllocationInfo(packages.getId(), PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<FundAllocationResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.hideRedemptionLoading();
                    }

                    @Override
                    public void onNext(FundAllocationResponse response) {

                        if (response.getCode() == 1) {
                            if (response.getData() != null &&
                                    response.getData().get(0) != null) {
                                FundAllocation allocation = response.getData().get(0);
                                fragment.addDownloadHandler(allocation);
                            } else {
                                fragment.removeDownloadHandler();
                            }
                            fragment.hideRedemptionLoading();
                        } else {
                            fragment.removeDownloadHandler();
                            fragment.hideRedemptionLoading();
                        }

                    }
                });

    }
}
