package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.RegisterNewCustomer;
import com.danareksa.investasik.data.api.beans.RegisterOnlineAccess;
import com.danareksa.investasik.data.api.requests.RegisterNasabahRequest;
import com.danareksa.investasik.ui.fragments.registernasabah.RegisterNasabahSyaratKetenFragment;

import butterknife.Bind;
import icepick.State;

/**
 * Created by asep.surahman on 08/05/2018.
 */

public class RegisterNasabahSyaratKetenActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    private static final String NASABAH_TYPE = "tipeNasabah";
    private static final String EMAIL = "email";
    private static final String REGISTER_NEW_REQUEST = "registerNewModel";
    private static final String REGISTER_ONLINE_REQUEST = "registerOnlineModel";

    @State
    String tipeNasabah;

    @State
    String email;

    @State
    RegisterNewCustomer registerNewCustomer;

    @State
    RegisterOnlineAccess registerOnlineAccess;

    @State
    RegisterNasabahRequest registerNasabahRequest;

    public static void startActivity(BaseActivity sourceActivity, String tipeNasabah, RegisterNewCustomer registerNewCustomer, String email){
        Intent intent = new Intent(sourceActivity, RegisterNasabahSyaratKetenActivity.class);
        intent.putExtra(NASABAH_TYPE, tipeNasabah);
        intent.putExtra(EMAIL, email);
        intent.putExtra(REGISTER_NEW_REQUEST, registerNewCustomer);
        sourceActivity.startActivity(intent);
    }


    public static void startActivity(BaseActivity sourceActivity, String tipeNasabah, RegisterOnlineAccess registerOnlineAccess){
        Intent intent = new Intent(sourceActivity, RegisterNasabahSyaratKetenActivity.class);
        intent.putExtra(NASABAH_TYPE, tipeNasabah);
        intent.putExtra(REGISTER_ONLINE_REQUEST, registerOnlineAccess);
        sourceActivity.startActivity(intent);
    }

    public static void startActivity(BaseActivity sourceActivity, String tipeNasabah, RegisterNasabahRequest registerNasabahRequest){
        Intent intent = new Intent(sourceActivity, RegisterNasabahSyaratKetenActivity.class);
        intent.putExtra(NASABAH_TYPE, tipeNasabah);
        intent.putExtra(REGISTER_ONLINE_REQUEST, registerNasabahRequest);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_register_data_pribadi;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setupToolbar();
        title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLts.otf"));
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        tipeNasabah = getIntent().getStringExtra(NASABAH_TYPE);
        email = getIntent().getStringExtra(EMAIL);

        if(getIntent().getSerializableExtra(REGISTER_NEW_REQUEST) != null){
            registerNewCustomer = (RegisterNewCustomer) getIntent().getSerializableExtra(REGISTER_NEW_REQUEST);
        }

        if(getIntent().getSerializableExtra(REGISTER_ONLINE_REQUEST) != null){
//            registerOnlineAccess = (RegisterOnlineAccess) getIntent().getSerializableExtra(REGISTER_ONLINE_REQUEST);
            if(BuildConfig.FLAVOR.contentEquals("btn")){
                registerNasabahRequest = (RegisterNasabahRequest) getIntent().getSerializableExtra(REGISTER_ONLINE_REQUEST);
            } else {
                registerOnlineAccess = (RegisterOnlineAccess) getIntent().getSerializableExtra(REGISTER_ONLINE_REQUEST);
            }
        }

        if(tipeNasabah.equals("baru")){
            RegisterNasabahSyaratKetenFragment.showFragment(this, tipeNasabah, registerNewCustomer, email);
        }else if(tipeNasabah.equals("lama")){
//            RegisterNasabahSyaratKetenFragment.showFragment(this, tipeNasabah, registerOnlineAccess);
            if(BuildConfig.FLAVOR.contentEquals("btn")){
                RegisterNasabahSyaratKetenFragment.showFragment(this, tipeNasabah, registerNasabahRequest);
            } else {
                RegisterNasabahSyaratKetenFragment.showFragment(this, tipeNasabah, registerOnlineAccess);
            }
        }

    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        title.setText("Registrasi");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        //startActivity(new Intent(RegisterNasabahSyaratKetenActivity.this, RegisterNasabahActivity.class));
        finish();
    }


    public void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }


}
