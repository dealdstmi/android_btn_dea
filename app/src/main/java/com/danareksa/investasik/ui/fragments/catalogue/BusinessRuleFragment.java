package com.danareksa.investasik.ui.fragments.catalogue;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.Fee;
import com.danareksa.investasik.data.api.beans.FundAllocation;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.RedemptionFee;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.DateUtil;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import icepick.State;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by fajarfatur on 1/22/16.
 */

@RuntimePermissions
public class BusinessRuleFragment extends BaseFragment {

    public static final String TAG = BusinessRuleFragment.class.getSimpleName();
    private static final String PACKAGES = "packages";

    @Bind(R.id.txvMinSubscription)
    TextView txvMinSubscription;
    @Bind(R.id.txvMinTopup)
    TextView txvMinTopup;
    @Bind(R.id.txvTransactionCutOff)
    TextView txvTransactionCutOff;
    @Bind(R.id.txvSettlementCutOff)
    TextView txvSettlementCutOff;
    @Bind(R.id.txvSettlementPeriod)
    TextView txvSettlementPeriod;
    @Bind(R.id.lnrSubscriptionData)
    LinearLayout lnrSubscriptionData;
    @Bind(R.id.lnrRedemptionData)
    LinearLayout lnrRedemptionData;
    @Bind(R.id.prbSubscription)
    ProgressBar prbSubscription;
    @Bind(R.id.prbRedemption)
    ProgressBar prbRedemption;
    @Bind(R.id.ivProspectus)
    ImageView ivProspectus;
    @Bind(R.id.ivFFS)
    ImageView ivFFS;

    private BusinessRulePresenter presenter;

    @State
    Packages packages;
    public List<Fee> subscriptionFees;
    public List<RedemptionFee> redemptionFees;

    NumberFormat nf;


    public static Fragment getFragment(Packages packages) {
        Fragment f = new BusinessRuleFragment();

        Bundle extras = new Bundle();
        extras.putSerializable(PACKAGES, packages);

        f.setArguments(extras);
        return f;
    }

    @Override
    protected int getLayout() {
        return R.layout.f_business_rules;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (packages == null) {
            Bundle extras = getArguments();
            if (extras != null && extras.containsKey(PACKAGES)) {
                packages = (Packages) extras.getSerializable(PACKAGES);
            }
        }

        if (packages != null) {
            presenter = new BusinessRulePresenter(this);
            presenter.redemptionFee(packages);
            presenter.subscriptionFee(packages);
            presenter.fundAllocation(packages);
            setDataToView();
        }

    }


    public void setDataToView() {
        nf = NumberFormat.getCurrencyInstance(new Locale("ID", "id"));
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);


        Calendar transCutOff = Calendar.getInstance();
        transCutOff.setTime(DateUtil.format(packages.getTransactionCutOff(), DateUtil.INVISEE_RETURN_FORMAT2));
        Calendar settlementCutOff = Calendar.getInstance();
        settlementCutOff.setTime(DateUtil.format(packages.getSettlementCutOff(), DateUtil.INVISEE_RETURN_FORMAT2));

        txvMinSubscription.setText(nf.format(packages.getMinSubscriptionAmount()).trim());
        txvMinTopup.setText(nf.format(packages.getMinTopupAmount()).trim());
        txvTransactionCutOff.setText(DateUtil.format(transCutOff.getTime(), DateUtil.HH_MM));
        txvSettlementCutOff.setText(DateUtil.format(settlementCutOff.getTime(), DateUtil.HH_MM));
        txvSettlementPeriod.setText("T+"+packages.getSettlementPeriod() + " " + "Hari" + " " + "Bursa");
    }


    public void showSubscriptionLoading() {
        prbSubscription.setVisibility(View.VISIBLE);
    }

    public void hideSubscriptionLoading() {
        prbSubscription.setVisibility(View.GONE);
    }

    public void showRedemptionLoading() {
        prbRedemption.setVisibility(View.VISIBLE);
    }

    public void hideRedemptionLoading() {
        prbRedemption.setVisibility(View.GONE);
    }

    public void setupSubscriptionView() {

        for (int i = 0; i < subscriptionFees.size(); i++) {

            Fee fee = subscriptionFees.get(i);

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.row_fee, null);

            String actualResult = MessageFormat.format("{0,number,#.##%}", fee.getFeeAmount());

            TextView txvItem = (TextView) view.findViewById(R.id.txvItem);
            TextView txvRate = (TextView) view.findViewById(R.id.txvRate);

            if (fee.getAmountMax() == 0) {
                txvItem.setText(" lebih dari " + nf.format(fee.getAmountMin()));

            } else {
                txvItem.setText(nf.format(fee.getAmountMin()) + " - " + nf.format(fee.getAmountMax()));
            }

            txvRate.setText(actualResult);

            lnrSubscriptionData.addView(view);
            lnrSubscriptionData.requestLayout();
            lnrSubscriptionData.invalidate();
        }
    }


    public void setupRedemptionView() {
        for (int i = 0; i < redemptionFees.size(); i++) {

            RedemptionFee fee = redemptionFees.get(i);

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.row_fee, null);

            String actualResult = MessageFormat.format("{0,number,#.##%}", fee.getFeeAmount());

            TextView txvItem = (TextView) view.findViewById(R.id.txvItem);
            TextView txvRate = (TextView) view.findViewById(R.id.txvRate);

            /*if (fee.getAmountMax() == 0) {
                txvItem.setText(" lebih dari " + fee.getAmountMin());
            } else {
                txvItem.setText(fee.getAmountMin() + " - " + fee.getAmountMax());
            }*/

            if (fee.getDayMax() == 0) {
                txvItem.setText(" lebih dari " + nf.format(fee.getDayMin()));
            } else {
                txvItem.setText(nf.format(fee.getDayMin()) + " - " + nf.format(fee.getDayMax()));
            }

            txvRate.setText(actualResult);

            lnrRedemptionData.addView(view);
            lnrRedemptionData.invalidate();
            lnrRedemptionData.requestLayout();
            lnrRedemptionData.getParent().requestLayout();
        }
    }

    public void addDownloadHandler(final FundAllocation allocation) {
        ivProspectus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadProspektus(allocation);
            }
        });

        ivFFS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadFfs(allocation);
            }
        });

    }

    public void removeDownloadHandler() {
        ivProspectus.setOnClickListener(null);
        ivFFS.setOnClickListener(null);
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void downloadFundFact(FundAllocation allocation) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(InviseeService.IMAGE_DOWNLOAD_URL + allocation.getFundFactSheetKey() + "&token=" + PrefHelper.getString(PrefKey.TOKEN)));
        request.setDescription("A download package with some files");
        request.setTitle("Fund Fact Sheet " + allocation.getProductName());
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.allowScanningByMediaScanner();
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Fund Fact Sheet " + allocation.getProductName()+".pdf");

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);

    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void downloadProscpectus(FundAllocation allocation) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(InviseeService.IMAGE_DOWNLOAD_URL + allocation.getProspectusKey() + "&token=" + PrefHelper.getString(PrefKey.TOKEN)));
        request.setDescription("A download package with some files");
        request.setTitle("Prospektus " + allocation.getProductName());
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.allowScanningByMediaScanner();
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Prospektus " + allocation.getProductName()+".pdf");

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }



    public void downloadProspektus(FundAllocation fundAllocData){
        BusinessRuleFragmentPermissionsDispatcher.downloadProscpectusWithPermissionCheck(BusinessRuleFragment.this, fundAllocData);
    }


    public void downloadFfs(FundAllocation fundAllocData){
        BusinessRuleFragmentPermissionsDispatcher.downloadFundFactWithPermissionCheck(BusinessRuleFragment.this, fundAllocData);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        BusinessRuleFragmentPermissionsDispatcher.onRequestPermissionsResult(this,requestCode,grantResults);
    }

}
