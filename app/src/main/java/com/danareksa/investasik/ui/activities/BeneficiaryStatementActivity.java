package com.danareksa.investasik.ui.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.fragments.tambahrekening.BeneficiaryStatementFragment;

import butterknife.Bind;

public class BeneficiaryStatementActivity  extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;
    String typeAccount = "";

    @Override
    protected int getLayout() {
        return R.layout.a_beneficiary_statement;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        typeAccount =  getIntent().getExtras().getString("type");
        setupToolbar();
        BeneficiaryStatementFragment.showFragment(this, typeAccount);
    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if (typeAccount.equals("REGULER")) {
            title.setText("Reguler");
        } else {
            title.setText("Lumpsum");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
