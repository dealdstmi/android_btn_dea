package com.danareksa.investasik.ui.fragments.tambahrekening;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;

import butterknife.Bind;
import butterknife.OnClick;

public class BeneficiaryStatementFragment  extends BaseFragment {
    public static final String TAG = BeneficiaryStatementFragment.class.getSimpleName();

    @Bind(R.id.btnOk)
    Button btnOk;

    @Bind(R.id.tvTerms)
    TextView tvTerms;

    @Override
    protected int getLayout() {
        return R.layout.f_beneficiary_statement;
    }

    public static void showFragment(BaseActivity sourceActivity, String type){
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();

            Bundle bundle = new Bundle();
            bundle.putString("type", type);
            BeneficiaryStatementFragment fragment = new BeneficiaryStatementFragment();
            fragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.container, fragment, TAG);

            fragmentTransaction.commit();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String type = getArguments().getString("type");
        if (type.equals("REGULER")) {
            tvTerms.setText(R.string.reguler_add_account_terms);
        }
    }

    @OnClick(R.id.btnOk)
    void btnOk(){
        this.getActivity().onBackPressed();
    }


}
