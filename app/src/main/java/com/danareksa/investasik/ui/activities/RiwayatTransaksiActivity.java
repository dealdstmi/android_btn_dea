package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.fragments.riwayattransaksi.RiwayatTransaksiFragment;

import butterknife.Bind;

/**
 * Created by asep.surahman on 29/06/2018.
 */

public class RiwayatTransaksiActivity extends BaseActivity{


    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;


    public static void startActivity(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, RiwayatTransaksiActivity.class);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_riwayat_transaksi;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        setTitle("");

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText("Riwayat Transaksi");
        RiwayatTransaksiFragment.showFragment(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        startActivity(new Intent(RiwayatTransaksiActivity.this, MainActivity.class));
        finish();
    }




}
