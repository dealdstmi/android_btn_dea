package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.PromoResponse;
import com.danareksa.investasik.ui.fragments.promo.DetailPromoFragment;

import butterknife.Bind;
import icepick.State;

/**
 * Created by pandu.abbiyuarsyah on 18/05/2017.
 */

public class DetailPromoActivity extends BaseActivity {

    private static final String PROMO = "promo";

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    @State
    PromoResponse response;

    public static void startActivity(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, DetailPromoActivity.class);
        sourceActivity.startActivity(intent);
    }

    public static void startActivity(BaseActivity sourceActivity, PromoResponse response) {
        Intent intent = new Intent(sourceActivity, DetailPromoActivity.class);
        intent.putExtra(PROMO, response);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_detail_promo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupToolbar();
        /*setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Detail Promo");
        }*/

        response = (PromoResponse) getIntent().getSerializableExtra(PROMO);
        /*title.setText(response.getName());*/

        DetailPromoFragment.showFragment(this, response);
    }


    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        title.setText("Detail Promo");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        if (IS_FROM_LANDING) {
            startActivity(new Intent(DetailPromoActivity.this, ArticleSlideshowActivity.class));
            DetailPromoActivity.IS_FROM_LANDING = false;
        } else {
            startActivity(new Intent(DetailPromoActivity.this, PromoActivity.class));
        }
        finish();
    }
}
