package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.LookupAddress;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by asep.surahman on 07/12/2018.
 */

public class CorrespondenceAdapter extends RecyclerView.Adapter<CorrespondenceAdapter.CorrespondenceHolder>{

    private Context context;
    private List<LookupAddress> list;

    public CorrespondenceAdapter(Context context, List<LookupAddress> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public CorrespondenceAdapter.CorrespondenceHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_statement_line, parent, false);
        CorrespondenceAdapter.CorrespondenceHolder catalogueHolder = new CorrespondenceAdapter.CorrespondenceHolder(itemView);
        return catalogueHolder;
    }

    @Override
    public void onBindViewHolder(CorrespondenceAdapter.CorrespondenceHolder holder, int position){
        final LookupAddress item = list.get(position);
        holder.itemView.setTag(item);
        holder.tvValue.setText(item.getCode());
        holder.tvDesc.setText(item.getValue());
        if(item.isStatus() == true){
            holder.tvValue.setText(getCheckedIcon());
            holder.tvValue.setEnabled(true);
        }

    }


    @Override
    public int getItemCount(){
        return list != null ? list.size() : 0;
    }


    public static class CorrespondenceHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvValue)
        TextView tvValue;
        @Bind(R.id.tvDesc)
        TextView tvDesc;
        @Bind(R.id.llAddress)
        LinearLayout llAddress;
        public CorrespondenceHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


    private Spannable getCheckedIcon(){
        Spannable buttonLabel = new SpannableString(" ");
        buttonLabel.setSpan(new ImageSpan(context.getApplicationContext(), R.drawable.ic_checked, ImageSpan.ALIGN_BOTTOM), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return  buttonLabel;
    }




}
