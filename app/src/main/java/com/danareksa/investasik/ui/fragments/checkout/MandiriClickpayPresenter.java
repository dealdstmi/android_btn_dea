package com.danareksa.investasik.ui.fragments.checkout;
import android.graphics.Color;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.MandiriClickpayDetail;
import com.danareksa.investasik.data.api.requests.MandiriClickpayRequest;
import com.danareksa.investasik.data.api.responses.TrxTransMandiriClickPayResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.CartActivity;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 27/08/2018.
 */

public class MandiriClickpayPresenter {


    private MandiriClickpayFragment fragment;


    public MandiriClickpayPresenter(MandiriClickpayFragment fragment) {
        this.fragment = fragment;
    }


    void getPaymentMandiriClickpay(String paymentMethod, MandiriClickpayDetail paymentDetails) {
        fragment.showProgressBar();
        fragment.getApi().mandiriClickpay(getMandiriClickpayRequest(paymentMethod, paymentDetails))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<TrxTransMandiriClickPayResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(TrxTransMandiriClickPayResponse response) {
                        fragment.dismissProgressBar();
                        if (response.getCode() == 0) {
                            fragment.trxTransCode = response.getData();
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.transaksi_success))
                                    .titleColor(Color.BLACK)
                                    .content(R.string.transaksi_success_desc)
                                    .contentColor(Color.GRAY)
                                    .contentGravity(GravityEnum.CENTER)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            fragment.fetchResultToFragment();
                                            dialog.dismiss();
                                        }
                                    })
                                    .cancelable(false)
                                    .show();
                        }else{
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.transaksi_failed).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(response.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                            CartActivity.startActivity((BaseActivity) fragment.getActivity());
                                        }
                                    })
                                    .show();
                        }
                    }

                });
    }



    public MandiriClickpayRequest getMandiriClickpayRequest(String paymentMethod, MandiriClickpayDetail paymentDetails){
        MandiriClickpayRequest mandiriClickpayRequest = new MandiriClickpayRequest();
        mandiriClickpayRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        mandiriClickpayRequest.setAppsType("ANDROID");
        mandiriClickpayRequest.setPaymentMethod(paymentMethod);
        mandiriClickpayRequest.setDetail(paymentDetails);
        return  mandiriClickpayRequest;
    }


    public MandiriClickpayDetail getMandiriClickpayModel(String cardNumber, int idCart, String mandiriToken, int netAmount){
        MandiriClickpayDetail mandiriClickpayDetail = new MandiriClickpayDetail();
        mandiriClickpayDetail.setDebitCardNumber(cardNumber);
        mandiriClickpayDetail.setId(idCart);
        mandiriClickpayDetail.setMandiriClickpayToken(mandiriToken);
        mandiriClickpayDetail.setNetAmount(netAmount);
        return  mandiriClickpayDetail;
    }





}
