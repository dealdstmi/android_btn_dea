package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.TrxTransCode;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.util.AmountFormatter;
import com.danareksa.investasik.util.Constant;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by asep.surahman on 12/08/2018.
 */

public class SummaryPaymentTransferAdapter extends RecyclerView.Adapter<SummaryPaymentTransferAdapter.SummaryPaymentTransferHolder>{


    List<TrxTransCode> trxTransCodeList;
    private TrxTransCode trxTransCode;
    private Context context;
    private List<SummaryPaymentTransferAdapter.SummaryPaymentTransferHolder> holderList;
    double totAmount = 0;
    String paymentType = "";



    public SummaryPaymentTransferAdapter(Context context, List<TrxTransCode> trxTransCodeList, String paymentType) {
        this.context = context;
        this.trxTransCodeList = trxTransCodeList;
        this.paymentType = paymentType;
        this.holderList = new ArrayList<>();
    }


    @Override
    public SummaryPaymentTransferAdapter.SummaryPaymentTransferHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_summary_payment_transfer, parent, false);
        return new SummaryPaymentTransferAdapter.SummaryPaymentTransferHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final SummaryPaymentTransferAdapter.SummaryPaymentTransferHolder holder, final int position) {
        trxTransCode = trxTransCodeList.get(position);
        holder.itemView.setTag(trxTransCode);

        Picasso.with(context).load(InviseeService.IMAGE_DOWNLOAD_URL + trxTransCode.getPackageImageKey()  + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                .placeholder(R.drawable.ic_logo_launcher)
                .error(R.drawable.ic_logo_launcher)
                .into(holder.icon);

        if(trxTransCode.getTransactionTypeCode().equals("SUBCR")){
            holder.txvRekeningInvestasi.setText("Pembelian - " + trxTransCode.getIfuaType());
        }else if(trxTransCode.getTransactionTypeCode().equals("TOPUP")){
            holder.txvRekeningInvestasi.setText("Pembelian - " + trxTransCode.getIfuaType());
        }
        holder.txvOrderNumber.setText(trxTransCode.getOrderNumber());
        holder.txvKodeInvestor.setText(trxTransCode.getIfuaNumber() + " - " + trxTransCode.getIfuaInvestmentGoalName());
        holder.txvMetodePembayaran.setText(trxTransCode.getPaymentMethodName());
        holder.tvPackageName.setText(trxTransCode.getPackageName());
        holder.tvDestinationAccountBank.setText(trxTransCode.getDestinationAccountBankName());

        if(paymentType.equals(Constant.PAYMENT_TYPE_VA_PERMATA)){
            holder.tvNomorRekening.setText("Nomor VA");
        }else if(paymentType.equals(Constant.PAYMENT_TYPE_TRANS_BCA)){
            holder.tvNomorRekening.setText("Nomor Rekening BCA");
        }else if(paymentType.equals(Constant.PAYMENT_TYPE_TRANS_MANDIRI)){
            holder.tvNomorRekening.setText("Nomor Rekening Mandiri");
        }

        if(trxTransCode.getNetAmount() != 0.0 && trxTransCode.getNetAmount() != null){
            holder.tvAmount.setText(String.valueOf(AmountFormatter.formatCurrencyWithoutComma(trxTransCode.getNetAmount())));
        }else{
            holder.tvAmount.setText("0");
        }

        if(trxTransCode.getFeeAmount() != 0.0 && trxTransCode.getFeeAmount() != null){

            double feeAmount  = trxTransCode.getFeeAmount();
            System.out.println("f percent payment : " + trxTransCode.getFeePercentage()/100);
            holder.tvFee.setText(AmountFormatter.formatFeePercent(trxTransCode.getFeePercentage()/100) + " | " + AmountFormatter.formatNonCurrency(feeAmount));

        }else{
            holder.tvFee.setText("0% | 0");
        }

        if(trxTransCode.getTotalAmount() != 0.0 && trxTransCode.getTotalAmount() != null){
            holder.tvTotal.setText(String.valueOf(AmountFormatter.formatCurrencyWithoutComma(trxTransCode.getNetAmount() + trxTransCode.getFeeAmount())));
        }else{
            holder.tvTotal.setText("0");
        }

        //totAmount = trxTransCode.getFeeAmount() + trxTransCode.getTotalAmount();
        if(trxTransCode.getTotalAmount() != 0.0 && trxTransCode.getTotalAmount() != null){
            holder.tvTotalAmount.setText(String.valueOf(AmountFormatter.formatCurrencyWithoutComma(Math.ceil((trxTransCode.getTotalAmount())))));
        }else{
            holder.tvTotalAmount.setText("0");
        }

        holder.tvAccountNumber.setText(trxTransCode.getDestinationAccountNumber());
        holder.tvAccountName.setText(trxTransCode.getDestinationAccountName());
        holderList.add(holder);
    }


    @Override
    public int getItemCount() {
        return trxTransCodeList != null ? trxTransCodeList.size() : 0;
    }


    public static class SummaryPaymentTransferHolder extends RecyclerView.ViewHolder {

        /*@Bind(R.id.icon)
        ImageView icon;
*/
        @Bind(R.id.icon)
        CircleImageView icon;

        @Bind(R.id.txvOrderNumber)
        TextView txvOrderNumber;
        @Bind(R.id.txvRekeningInvestasi)
        TextView txvRekeningInvestasi;
        @Bind(R.id.txvKodeInvestor)
        TextView txvKodeInvestor;
        @Bind(R.id.txvMetodePembayaran)
        TextView txvMetodePembayaran;
        @Bind(R.id.tvPackageName)
        TextView tvPackageName;
        @Bind(R.id.tvAmount)
        TextView tvAmount;
        @Bind(R.id.tvTotal)
        TextView tvTotal;
        @Bind(R.id.tvTotalAmount)
        TextView tvTotalAmount;
        @Bind(R.id.tvAccountNumber)
        TextView tvAccountNumber;
        @Bind(R.id.tvAccountName)
        TextView tvAccountName;
        @Bind(R.id.tvFee)
        TextView tvFee;
        @Bind(R.id.tvNomorRekening)
        TextView tvNomorRekening;
        @Bind(R.id.tvAtasNama)
        TextView tvAtasNama;
        @Bind(R.id.tvDestinationAccountBank)
        TextView tvDestinationAccountBank;


        public SummaryPaymentTransferHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
