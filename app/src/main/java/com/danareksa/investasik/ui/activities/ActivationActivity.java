package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.fragments.activation.ActivationAccountFragment;

import butterknife.Bind;
import butterknife.BindString;

/**
 * Created by fajarfatur on 2/3/16.
 */

public class ActivationActivity extends BaseActivity {

    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;
    @BindString(R.string.activation_title)
    String activationTitle;

    private String username;
    private String password;
    private String email;


    public static void startActivity(BaseActivity sourceActivity, String username, String password, String email) {
        Intent intent = new Intent(sourceActivity, ActivationActivity.class);
        intent.putExtra(USERNAME, username);
        intent.putExtra(PASSWORD, password);
        intent.putExtra(EMAIL, email);
        sourceActivity.startActivity(intent);
    }

    public static void startActivity(BaseActivity sourceActivity, String username, String password) {
        Intent intent = new Intent(sourceActivity, ActivationActivity.class);
        intent.putExtra(USERNAME, username);
        intent.putExtra(PASSWORD, password);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout(){
        return R.layout.a_register_data_pribadi;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        title.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLts.otf"));
        title.setText(activationTitle);
        setSupportActionBar(toolbar);
        setTitle("");

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().hasExtra(USERNAME)) {
            username = getIntent().getStringExtra(USERNAME);
            password = getIntent().getStringExtra(PASSWORD);
            email= getIntent().getStringExtra(EMAIL);
            ActivationAccountFragment.showFragment(this, username, password, email);
        } else {
            Toast.makeText(this, "Can't find email to activate", Toast.LENGTH_SHORT).show();
            finish();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ActivationActivity.this, SignInActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
