package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.TransactionHistory;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.DetailPendingOrderActivity;
import com.danareksa.investasik.ui.fragments.dashboard.PendingOrderPresenter;
import com.danareksa.investasik.util.AmountFormatter;
import com.danareksa.investasik.util.DateUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by pandu.abbiyuarsyah on 30/03/2017.
 */

public class PendingOrderAdapter extends RecyclerView.Adapter<PendingOrderAdapter.PendingOrderHolder>  {

    ArrayList<TransactionHistory> list;

    Context context;
    PendingOrderPresenter presenter;


    public PendingOrderAdapter(ArrayList<TransactionHistory> list, Context context, PendingOrderPresenter presenter) {
        this.list = list;
        this.context = context;
        this.presenter = presenter;
    }


    @Override
    public PendingOrderAdapter.PendingOrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pending_order, parent, false);
        return new PendingOrderAdapter.PendingOrderHolder(itemView);
    }

        @Override
        public void onBindViewHolder(PendingOrderAdapter.PendingOrderHolder holder, int position) {
            final TransactionHistory item = list.get(position);

        holder.txvPackageName.setText(item.getPackageName());
        holder.txvTransactionType.setText(item.getTransactionType());
        holder.txvOrderNumber.setText(item.getOrderNumber());
        holder.txvInvestmentNumber.setText(item.getInvestmentNumber());
        holder.txvTotal.setText(AmountFormatter.format(item.getAmount()));
        holder.txvTransactionStatus.setText(item.getTransactionStatus());
        holder.txvTime.setText(item.getTransactionTime());

        SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.INVISEE_RETURN_FORMAT2,    Locale.getDefault());
        Date dateParse = null;
        try {
            dateParse = sdf.parse(item.getTransactionDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy" , Locale.getDefault());
        String convertedDate= formatter.format(dateParse);
        holder.txvDate.setText(convertedDate);
        holder.lnItemOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailPendingOrderActivity.startActivity((BaseActivity) context, item);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class PendingOrderHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.txvDate)
        TextView txvDate;
        @Bind(R.id.txvTime)
        TextView txvTime;
        @Bind(R.id.txvTransactionType)
        TextView txvTransactionType;
        @Bind(R.id.txvPackageName)
        TextView txvPackageName;
        @Bind(R.id.txvOrderNumber)
        TextView txvOrderNumber;
        @Bind(R.id.txvInvestmentNumber)
        TextView txvInvestmentNumber;
        @Bind(R.id.txvTotal)
        TextView txvTotal;
        @Bind(R.id.txvTransactionStatus)
        TextView txvTransactionStatus;
        @Bind(R.id.lnItemOrder)
        LinearLayout lnItemOrder;

        public PendingOrderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }



}
