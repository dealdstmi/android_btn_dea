package com.danareksa.investasik.ui.fragments.tambahrekening;

import android.widget.Toast;

import com.danareksa.investasik.data.api.beans.Bank;
import com.danareksa.investasik.data.api.beans.Country;
import com.danareksa.investasik.data.api.requests.AddAccountLumpsumRequest;
import com.danareksa.investasik.data.api.requests.AddAccountRegulerRequest;
import com.danareksa.investasik.data.api.requests.AddAccountRequest;
import com.danareksa.investasik.data.api.requests.CountryRequest;
import com.danareksa.investasik.data.api.requests.KycLookupRequest;
import com.danareksa.investasik.data.api.responses.InvestAccountResponse;
import com.danareksa.investasik.data.api.responses.KycLookupResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.data.realm.RealmHelper;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.SummaryInvestAccountActivity;
import com.danareksa.investasik.util.Constant;
import com.google.gson.Gson;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by asep.surahman on 06/08/2018.
 */

public class InvestAccountMainPresenter {

    private static final String MAX = "999";
    private static final String OFFSET = "0";

    InvestAccountMainFragment fragment;
    public List<Country> countries;

    public InvestAccountMainPresenter(InvestAccountMainFragment fragment){
        this.fragment = fragment;
    }


    void getKycLookupData(final String typeAccount){
        fragment.showProgressBar();
        if (!isKycLookupAlreadyOnRealm()){
            fragment.getApi().getKycLookupNew(getKycLookupRequest())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Observer<KycLookupResponse>(){
                        @Override
                        public void onCompleted(){

                        }

                        @Override
                        public void onError(Throwable e){
                            fragment.dismissProgressBar();
                            fragment.connectionError();
                        }

                        @Override
                        public void onNext(KycLookupResponse kycLookupResponse){
                            RealmHelper.createKycLookup(fragment.getRealm(), kycLookupResponse.getKycLookupList());
                            getAllBank();
                        }
                    });
        }else{
            getAllBank();
        }
    }



    public void getAllBank() {
        fragment.getApi().getAllBank(MAX, OFFSET, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Bank>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(List<Bank> banks) {
                        fragment.banks = banks;
                        if (banks != null && banks.size() > 0) {
                            getAllCountry(banks);
                        }
                    }
                });
    }



    public void getAllCountry(final List<Bank> listBank){
        fragment.getApi().getAllCountryNew(getCountryRequest())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Country>>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(List<Country> countries){
                        fragment.dismissProgressBar();
                        fragment.countries = countries;
                        String jsonCountry = new Gson().toJson(countries);
                        String jsonBank = new Gson().toJson(listBank);

                        if (countries.size() > 0) {
                            fragment.setUpAdapter(jsonCountry, jsonBank);
                            fragment.dismissProgressBar();
                        } else {
                            fragment.setUpAdapter(jsonCountry, jsonBank);
                            fragment.dismissProgressBar();
                        }

                    }
                });
    }



    private boolean isKycLookupAlreadyOnRealm(){
        return RealmHelper.getKycLookupSize(fragment.getRealmInvest()) > 0;
    }


    private CountryRequest getCountryRequest(){
        CountryRequest countryRequest = new CountryRequest();
        countryRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        return countryRequest;
    }


    private KycLookupRequest getKycLookupRequest(){
        KycLookupRequest kycLookupRequest = new KycLookupRequest();
        kycLookupRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        return kycLookupRequest;
    }


    private AddAccountRegulerRequest getAddAccountRegulerRequest(AddAccountRequest req){
        Timber.i("Submitted Reguler Data To WS: %s", req);
        AddAccountRegulerRequest addAccountReg = new AddAccountRegulerRequest();
        addAccountReg.setToken(PrefHelper.getString(PrefKey.TOKEN));
        addAccountReg.setAccountTypeCode("REG");
        addAccountReg.setGoalCode(req.getGoalCode());
        addAccountReg.setGoalDetail(req.getGoalDetail());
        addAccountReg.setApssType("ANDROID");
        addAccountReg.setBankAccountId(req.getBankAccountId());
        addAccountReg.setHeirList(req.getHeirList());
        addAccountReg.setPackageList(req.getPackageList());
        addAccountReg.setBankId(req.getBankId());
        addAccountReg.setCountryId(req.getCountryId());
        addAccountReg.setBankAccountName(req.getBankAccountName());
        addAccountReg.setBankBranchId(req.getBankBranchId());
        addAccountReg.setBankAccountNumber(req.getBankAccountNumber());
        addAccountReg.setBankAccountImage(req.getBankAccountImage());
        return addAccountReg;
    }


    private AddAccountLumpsumRequest getAddAccountLumpsumRequest(AddAccountRequest req){
        Timber.i("Submitted Lumpsum Data To WS: %s", req);
        AddAccountLumpsumRequest addAccountLumpsum = new AddAccountLumpsumRequest();
        addAccountLumpsum.setToken(PrefHelper.getString(PrefKey.TOKEN));
        addAccountLumpsum.setAccountTypeCode("LMP");
        addAccountLumpsum.setGoalCode(req.getGoalCode());
        addAccountLumpsum.setGoalDetail(req.getGoalDetail());
        addAccountLumpsum.setApssType("ANDROID");
        addAccountLumpsum.setBankId(req.getBankId());
        addAccountLumpsum.setCountryId(req.getCountryId());
        addAccountLumpsum.setBankAccountName(req.getBankAccountName());
        addAccountLumpsum.setBankAccountNumber(req.getBankAccountNumber());
        addAccountLumpsum.setBankAccountImage(req.getBankAccountImage());
        addAccountLumpsum.setBankAccountId(req.getBankAccountId());
        addAccountLumpsum.setBankBranchId(req.getBankBranchId());
        addAccountLumpsum.setHeirList(req.getHeirList());
        return addAccountLumpsum;
    }


    void saveAccountReguler(AddAccountRequest addAccountRegulerRequest){
        fragment.showProgressBar();
        fragment.getApi().submitAccountReguler(getAddAccountRegulerRequest(addAccountRegulerRequest))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<InvestAccountResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(InvestAccountResponse investAccountResponse){
                        fragment.dismissProgressBar();
                        if(investAccountResponse.getCode() == 0){

                            SummaryInvestAccountActivity.startActivity((BaseActivity) fragment.getActivity(), investAccountResponse.getInvestAccount(), Constant.INVEST_TYPE_REGULER);
                            fragment.getActivity().finish();

                        }else{
                            Toast.makeText(fragment.getActivity(), investAccountResponse.getInfo(), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }


    void saveAccountLumpsum(AddAccountRequest addAccountLumpsumRequest){
        fragment.showProgressBar();
        fragment.getApi().submitAccountLumpsum(getAddAccountLumpsumRequest((addAccountLumpsumRequest)))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<InvestAccountResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(InvestAccountResponse investAccountResponse){
                        fragment.dismissProgressBar();
                        if(investAccountResponse.getCode() == 0){


                            SummaryInvestAccountActivity.startActivity((BaseActivity) fragment.getActivity(), investAccountResponse.getInvestAccount(), Constant.INVEST_TYPE_LUMPSUM);
                            fragment.getActivity().finish();

                        }else{
                            Toast.makeText(fragment.getActivity(), investAccountResponse.getInfo(), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }



}
