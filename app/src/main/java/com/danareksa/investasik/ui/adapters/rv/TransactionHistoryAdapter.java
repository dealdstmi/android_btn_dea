package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.TransactionHistory;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.util.AmountFormatter;
import com.danareksa.investasik.util.DateUtil;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by fajarfatur on 3/1/16.
 */
public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.HistoryHolder> {

    private Context context;
    private List<TransactionHistory> list;
    //private TransactionFragment mContext;

    public TransactionHistoryAdapter(Context context_, List<TransactionHistory> list) {
        this.context = context_;
        //this.mContext = context;
        this.list = list;
    }

    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_trx_history, parent, false);
        return new HistoryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final HistoryHolder holder, int position) {
        final TransactionHistory item  = list.get(position);
        holder.itemView.setTag(item);

        SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.INVISEE_RETURN_FORMAT2, Locale.getDefault());
        Date dateParse = null;
        try {
            dateParse = sdf.parse(item.getTransactionDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat(DateUtil.DD_MMM_YYYY, Locale.getDefault());
        String convertedDate = formatter.format(dateParse);

        holder.tvPackageName.setText(item.getPackageName());
        //holder.tvJumlah.setText(item.getInvestmentNumber());

        if (!item.getTransactionType().equals("REDEMPTION")) {

            if(item.getCurrency().equals("IDR")){
                if(item.getAmount() %2==0 || (item.getAmount()+1)%2==0){
                    holder.tvJumlah.setText(AmountFormatter.format(item.getAmount())); //bilangan bulat
                }else{
                    holder.tvJumlah.setText(AmountFormatter.format(Math.ceil(item.getAmount()))); //bilangan decimal
                }
            }else if(item.getCurrency().equals("USD")){
                if(item.getAmount() %2==0 || (item.getAmount()+1)%2==0){
                    holder.tvJumlah.setText(AmountFormatter.formatUsd(item.getAmount()));
                }else{
                    holder.tvJumlah.setText(AmountFormatter.formatUsd(Math.ceil(item.getAmount())));
                }
            }

        } else {
            holder.tvJumlah.setText(AmountFormatter.format(item.getAmount()));
        }

        holder.tvOrderNumber.setText(item.getOrderNumber());
        holder.tvTrxDate.setText(convertedDate);
        holder.tvTrxStatus.setText(item.getTransactionStatus());

        Picasso.with(context).load(InviseeService.IMAGE_DOWNLOAD_URL + item.getPkg_image() + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                .placeholder(R.drawable.ic_logo_launcher)
                .error(R.drawable.ic_logo_launcher)
                .into(holder.icon);

    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class HistoryHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvPackageName)
        TextView tvPackageName;
        @Bind(R.id.tvOrderNumber)
        TextView tvOrderNumber;
        @Bind(R.id.tvJumlah)
        TextView tvJumlah;
        @Bind(R.id.tvTrxDate)
        TextView tvTrxDate;
        @Bind(R.id.tvTrxStatus)
        TextView tvTrxStatus;
        @Bind(R.id.icon)
        CircleImageView icon;

        public HistoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }



}