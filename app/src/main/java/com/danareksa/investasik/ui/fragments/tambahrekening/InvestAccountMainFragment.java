package com.danareksa.investasik.ui.fragments.tambahrekening;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Bank;
import com.danareksa.investasik.data.api.beans.Country;
import com.danareksa.investasik.data.api.requests.AddAccountRequest;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.adapters.pager.InvestmentAccountLumpsumAdapter;
import com.danareksa.investasik.ui.adapters.pager.InvestmentAccountPagerAdapter;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.danareksa.investasik.util.ui.NonSwipeableViewPager;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by asep.surahman on 06/08/2018.
 */

public class InvestAccountMainFragment extends BaseInvest {

    public static final String TAG = InvestAccountMainFragment.class.getSimpleName();
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.bNext)
    Button bNext;
    @Bind(R.id.llNext)
    LinearLayout llNext;
    private long mLastClickTime = 0;

    @State
    int pageNumber = 0;
    @Bind(R.id.pager)
    NonSwipeableViewPager pager;

    public List<Country> countries;
    public List<Bank> banks;

    @State
    String typeAccount;

    @State
    int countPagerAdapter;

    @State
    AddAccountRequest addAccountRequest;

    private InvestmentAccountPagerAdapter pagerAdapter;
    private InvestmentAccountLumpsumAdapter pagerAdapterLumpsum;
    private InvestAccountMainPresenter presenter;
    LayoutInflater inflater;


    public static void showFragment(BaseActivity sourceActivity, String type){
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            Bundle bundle = new Bundle();
            bundle.putString("type", type);
            InvestAccountMainFragment fragment = new InvestAccountMainFragment();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }



    @Override
    protected int getLayout(){
        return R.layout.f_select_investment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        typeAccount = getActivity().getIntent().getStringExtra("type");
        presenter = new InvestAccountMainPresenter(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        addAccountRequest = new AddAccountRequest();
        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        presenter.getKycLookupData(typeAccount);
    }



    @Override
    public void busHandler(RxBusObject.RxBusKey busKey, Object busObject){
        super.busHandler(busKey, busObject);
        switch (busKey){
            case NEXT_FORM:
                toNextForm();
                break;
            case TO_PAGE_HEIR:
                toPageHeir();
                break;
            case SAVE_INVEST_REGULER:
                addAccountRequest = (AddAccountRequest) busObject;
                presenter.saveAccountReguler(addAccountRequest);
                break;
            case SAVE_INVEST_LUMPSUM:
                addAccountRequest = (AddAccountRequest) busObject;
                presenter.saveAccountLumpsum(addAccountRequest);
                break;
            case BACK_TO_PAGE:
                toPreviewsForm();
                break;
            case TO_PAGE_BEFORE_HEIR:
                toPageBeforeHeir();
                break;
            case FINISH_STEP:
                finishStep();
                break;
        }
    }

    private void finishStep(){
        getActivity().finish();
    }



    public void toPreviewsForm(){
        if(typeAccount.equals(Constant.INVEST_TYPE_REGULER)){
            countPagerAdapter = pagerAdapter.getCount()-1;
        }else if(typeAccount.equals(Constant.INVEST_TYPE_LUMPSUM)){
            countPagerAdapter = pagerAdapterLumpsum.getCount()-1;
        }

        if(pageNumber < countPagerAdapter){
            pageNumber--;
            pager.setCurrentItem(pageNumber);
            setTextNavigation(pageNumber);
        }
    }

    public void toNextForm(){

        if(typeAccount.equals(Constant.INVEST_TYPE_REGULER)){
            countPagerAdapter = pagerAdapter.getCount()-1;
        }else if(typeAccount.equals(Constant.INVEST_TYPE_LUMPSUM)){
            countPagerAdapter = pagerAdapterLumpsum.getCount()-1;
        }

        if(pageNumber < countPagerAdapter){
            pageNumber++;
            pager.setCurrentItem(pageNumber);
            setTextNavigation(pageNumber);
        }

    }


    public void toPageHeir(){

        if(typeAccount.equals(Constant.INVEST_TYPE_REGULER)){
            countPagerAdapter = pagerAdapter.getCount()-1;
            if(pageNumber < countPagerAdapter){
                pageNumber = 4; //4 == page Heir in Reguler
                pager.setCurrentItem(pageNumber);
                setTextNavigation(pageNumber);
            }
        }else if(typeAccount.equals(Constant.INVEST_TYPE_LUMPSUM)){
            countPagerAdapter = pagerAdapterLumpsum.getCount()-1;
            if(pageNumber < countPagerAdapter){
                pageNumber = 3; //3 == page Heir in Lumpsum
                pager.setCurrentItem(pageNumber);
                setTextNavigation(pageNumber);
            }
        }

    }



    public void toPageBeforeHeir(){
        if(typeAccount.equals(Constant.INVEST_TYPE_REGULER)){
            countPagerAdapter = pagerAdapter.getCount()-1;
            System.out.println("page number : " + pageNumber + "  - curren pager : " + countPagerAdapter);
            if(pageNumber <= countPagerAdapter){
                pageNumber = 2; //page select bank account in reguler
                pager.setCurrentItem(pageNumber);
                setTextNavigation(pageNumber);
            }
        }else if(typeAccount.equals(Constant.INVEST_TYPE_LUMPSUM)){
            countPagerAdapter = pagerAdapterLumpsum.getCount()-1;
            if(pageNumber <= countPagerAdapter){
                pageNumber = 1; //page select bank account in Lumpsum
                pager.setCurrentItem(pageNumber);
                setTextNavigation(pageNumber);
            }

        }
    }


    @OnClick(R.id.bNext)
    void bNext(){
        if(SystemClock.elapsedRealtime() - mLastClickTime < 1000){return;}
        mLastClickTime = SystemClock.elapsedRealtime();
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.GET_NEW_INPUTTED_INVEST, null));
    }


    public void setUpAdapter(String jsonCountry, String jsonBank){

        if(pageNumber == 0){
            bNext.setVisibility(View.GONE);
            llNext.setVisibility(View.GONE);
        }

        if(typeAccount.equals(Constant.INVEST_TYPE_REGULER)){
            pagerAdapter = new InvestmentAccountPagerAdapter(this, getChildFragmentManager(), typeAccount, jsonCountry, jsonBank, addAccountRequest);
            pager.setAdapter(pagerAdapter);
        }else if(typeAccount.equals(Constant.INVEST_TYPE_LUMPSUM)){
            pagerAdapterLumpsum = new InvestmentAccountLumpsumAdapter(this, getChildFragmentManager(), typeAccount, jsonCountry, jsonBank, addAccountRequest);
            pager.setAdapter(pagerAdapterLumpsum);
        }
    }

    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError(){
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }


    private void setTextNavigation(int current){
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        switch (current) {
            case 0:
                bNext.setVisibility(View.GONE);
                llNext.setVisibility(View.GONE);
                break;
            case 1:
                bNext.setVisibility(View.GONE);
                llNext.setVisibility(View.GONE);
                break;
            case 2:
                bNext.setVisibility(View.GONE);
                llNext.setVisibility(View.GONE);
                break;
            case 3:
                bNext.setVisibility(View.GONE);
                llNext.setVisibility(View.GONE);
                break;
            case 4:
                bNext.setVisibility(View.GONE);
                llNext.setVisibility(View.GONE);
                break;
        }
    }


    @Override
    public void nextWithoutValidation(){
    }

    @Override
    public void saveDataKycWithBackpress(){
    }


    public void setupToolbarFromFragment(){
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(null);
        toolbar.removeAllViewsInLayout();
    }

    public void backToBefore(){
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TAMBAH_REKENING, null));
    }

    @Override
    public void addAccountBackpress(){

    }




}
