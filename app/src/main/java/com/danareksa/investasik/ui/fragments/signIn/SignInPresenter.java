package com.danareksa.investasik.ui.fragments.signIn;

import android.graphics.Color;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.requests.ForgotPasswordUserRequest;
import com.danareksa.investasik.data.api.requests.LoginRequest;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.api.responses.LoginResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.Crypto;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by fajarfatur on 2/2/16.
 */
public class SignInPresenter {

    private SignInFragment fragment;

    public SignInPresenter(SignInFragment fragment) {
        this.fragment = fragment;
    }

    LoginRequest constructLoginRequest() {
        LoginRequest request = new LoginRequest();
        String username = fragment.etUsername.getText().toString();
//        request.setPassword(Crypto.Encrypt(fragment.etPassword.getText().toString()));
        request.setPassword(fragment.etPassword.getText().toString());
        request.setUsername(username.toLowerCase());
        Timber.e("ini username : " + username);
        Timber.e("ini password : " + Crypto.Encrypt(fragment.etPassword.getText().toString()));
        return request;
    }

    void login(final LoginRequest loginRequest) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().loginUser(loginRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<LoginResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(final LoginResponse loginResponse) {
                        fragment.dismissProgressDialog();
                        if (loginResponse.getCode() == fragment.successCode){
                            if (loginResponse.getData().getUserStatus().equalsIgnoreCase(Constant.USER_STATUS_REGISTER)){
                                Toast.makeText(fragment.getContext(), loginResponse.getInfo(), Toast.LENGTH_SHORT).show();
                                fragment.gotoActivationCodeActivity(loginRequest.getUsername(), "", loginResponse.getData().getEmail());

                            } else if (loginResponse.getData().getUserStatus().equalsIgnoreCase(Constant.USER_STATUS_ACTIVE)) {
                                Toast.makeText(fragment.getContext(), loginResponse.getInfo(), Toast.LENGTH_SHORT).show();
                                saveCredential(loginResponse);
                                fragment.showUserProfileSuggestionDialog();

                            } else if (loginResponse.getData().getUserStatus().equalsIgnoreCase(Constant.USER_STATUS_PENDING)) {
                                Toast.makeText(fragment.getContext(), loginResponse.getInfo(), Toast.LENGTH_SHORT).show();
                                saveCredential(loginResponse);
                                fragment.gotoMainActivity();

                            } else {
                                Toast.makeText(fragment.getContext(), loginResponse.getInfo(), Toast.LENGTH_SHORT).show();
                                saveCredential(loginResponse);
                                fragment.gotoMainActivity();

                            }
                        } else if (loginResponse.getCode() == 99) {
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.infortmation).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(loginResponse.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            //urgentForgotPass(constructForgotPasswordRequest(loginResponse.getUser().getEmail()));
                                            dialog.dismiss();
                                        }
                                    })
                                    .cancelable(false)
                                    .show();

                        } else if (loginResponse.getCode() == 12) {

                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.infortmation).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(loginResponse.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.BLUE)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .cancelable(false)
                                    .show();
                        } else if (loginResponse.getCode() == 50 ) {
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.infortmation).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(loginResponse.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.BLUE)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .cancelable(false)
                                    .show();
                        } else  {
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.infortmation).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(loginResponse.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.BLUE)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .cancelable(false)
                                    .show();
                        }
                    }
                });
    }

    void urgentForgotPass(ForgotPasswordUserRequest forgotPasswordUserRequest) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().urgentForgotPassword(forgotPasswordUserRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNext(GenericResponse response) {
                        if (response.getCode() == 1) {
                            fragment.dismissProgressDialog();
                            RePasswordFragment.showFragment((BaseActivity) fragment.getActivity(), fragment.etUsername.getText().toString());
                        } else {
                            fragment.dismissProgressDialog();
                        }

                    }
                });
    }


    ForgotPasswordUserRequest constructForgotPasswordRequest(String email) {
        ForgotPasswordUserRequest request = new ForgotPasswordUserRequest();
        request.setEmail(email);
        return request;
    }

    private void saveCredential(LoginResponse loginResponse) {
        PrefHelper.setBoolean(PrefKey.IS_LOGIN, true);
        PrefHelper.setString(PrefKey.PREF_UNAME, loginResponse.getData().getUsername());
        PrefHelper.setInt(PrefKey.ID, loginResponse.getData().getId());
        PrefHelper.setInt(PrefKey.KYC_ID, loginResponse.getData().getKyc());
        PrefHelper.setString(PrefKey.EMAIL, loginResponse.getData().getEmail());
        PrefHelper.setString(PrefKey.CUSTOMER_STATUS, loginResponse.getData().getUserStatus());
        PrefHelper.setString(PrefKey.TOKEN, loginResponse.getData().getToken());
        String name = loginResponse.getData().getName().replace("  ", " ");
        PrefHelper.setString(PrefKey.FIRST_NAME, name);
        PrefHelper.setString(PrefKey.IMAGE, loginResponse.getData().getImage());
        PrefHelper.setString(PrefKey.NAME, loginResponse.getData().getName());
        PrefHelper.setString(PrefKey.CUSTOMER_STATUS, loginResponse.getData().getUserStatus());
        System.out.println("key image after login : " + loginResponse.getData().getImage());
    }

}
