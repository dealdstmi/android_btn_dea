package com.danareksa.investasik.ui.fragments.riwayattransaksi;

import com.danareksa.investasik.data.api.beans.InvestmentAccountInfo;
import com.danareksa.investasik.data.api.requests.InvestmentAcountGroupRequest;
import com.danareksa.investasik.data.api.responses.InvestmentAccountInfoResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.util.ArrayList;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 11/08/2018.
 */

public class RiwayatTransaksiPresenter {

    private RiwayatTransaksiFragment fragment;
    public RiwayatTransaksiPresenter(RiwayatTransaksiFragment transaksiFragment) {
        this.fragment = transaksiFragment;
    }

    public void getInvestmentAccountGroup(){
        fragment.showProgressBar();
        InvestmentAcountGroupRequest request = new InvestmentAcountGroupRequest();
        request.setToken(PrefHelper.getString(PrefKey.TOKEN));
        fragment.getApi().getInvestmentAccountGroup(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<InvestmentAccountInfoResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(InvestmentAccountInfoResponse investmentAccountInfoResponse) {
                        fragment.dismissProgressBar();
                        if(investmentAccountInfoResponse.getCode() == 0) {
                            ArrayList<InvestmentAccountInfo> lumpsum =  investmentAccountInfoResponse.getData().getLumpsum();
                            if (lumpsum == null) lumpsum = new ArrayList<>();

                            ArrayList<InvestmentAccountInfo> reguler =  investmentAccountInfoResponse.getData().getReguler();
                            if (reguler == null) reguler = new ArrayList<>();

                            lumpsum.addAll(reguler);

                            InvestmentAccountInfo nullInfo = new InvestmentAccountInfo();
                            nullInfo.setId(-1);
                            nullInfo.setIfua("Silahkan Pilih");
                            nullInfo.setUnitHolderNumber("");
                            nullInfo.setInvestmentGoal("");

                            lumpsum.add(0, nullInfo);

                            fragment.listIFUA = lumpsum;

                            fragment.setupSpinner();
                            fragment.status = 1;
                        }else{
                            fragment.setupSpinner();
                            fragment.status = 0;
                        }

                    }
                });
    }

}
