package com.danareksa.investasik.ui.fragments.tambahrekening;

import com.danareksa.investasik.data.api.beans.Bank;
import com.danareksa.investasik.data.api.beans.Branch;
import com.danareksa.investasik.data.api.requests.BankListRequest;
import com.danareksa.investasik.data.api.responses.BankListResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 06/08/2018.
 */

public class AccountDataPresenter {

    AccountDataFragment fragment;
    private static final String MAX = "999";
    private static final String OFFSET = "0";

    public AccountDataPresenter(AccountDataFragment fragment){
        this.fragment = fragment;
    }

    public void getBranch(final String bankId){
        //fragment.showProgressBar();
        fragment.getApi().getListBranchByBankCodeNew(bankId, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Branch>>(){
                    @Override
                    public void onCompleted(){
                    }

                    @Override
                    public void onError(Throwable e){
                        //fragment.dismissProgressBar();
                        //fragment.connectionError();
                    }

                    @Override
                    public void onNext(List<Branch> branches){
                        //fragment.dismissProgressBar();
                        if (branches == null)
                            branches = new ArrayList<>();

                        Branch otherBranch = new Branch();
                        otherBranch.setId(-1);
                        otherBranch.setBranchName("Lainnya");

                        branches.add(0, otherBranch);

                        fragment.branches = branches;
                        fragment.fetchResultBranchToLayout();
                    }
                });

    }


    public BankListRequest getListRequest(){
        BankListRequest bankListRequest = new BankListRequest();
        bankListRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        return bankListRequest;
    }


    public void getBankList(){
        //fragment.showProgressBar();
        fragment.getApi().getBankListReguler(getListRequest())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<BankListResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        //fragment.dismissProgressBar();
                        //fragment.connectionError();
                    }

                    @Override
                    public void onNext(BankListResponse bankListResponse){
                        //fragment.dismissProgressBar();
                        fragment.bankList = bankListResponse.getData();
                        fragment.fetchResultBankToLayout();
                    }
                });

    }



    public void getAllBank() {
        fragment.getApi().getAllBank(MAX, OFFSET, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Bank>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        //fragment.connectionError();
                        //fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(List<Bank> banks) {
                        fragment.bankList = banks;
                        fragment.fetchResultBankToLayout();
                    }
                });
    }





}
