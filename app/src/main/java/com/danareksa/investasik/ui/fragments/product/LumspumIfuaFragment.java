package com.danareksa.investasik.ui.fragments.product;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.CategoryPortfolio;
import com.danareksa.investasik.data.api.beans.InvestmentAccountInfo;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.ProductList;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.adapters.rv.CategoryPortfolioAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.ui.RecyclerItemClickListener;

import java.util.List;

import butterknife.Bind;
import icepick.State;

/**
 * Created by asep.surahman on 11/06/2018.
 */

public class LumspumIfuaFragment extends BaseFragment{

    public static final String TAG = LumspumIfuaFragment.class.getSimpleName();

    @Bind(R.id.llPortfolio)
    LinearLayout llPortfolio;
    @Bind(R.id.llNoPortfolio)
    LinearLayout llNoPortfolio;
    @Bind(R.id.rv)
    RecyclerView rv;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;

    List<CategoryPortfolio> categoryPortfolios;
    public  List<InvestmentAccountInfo> lumpsum;

    private final static String PRODUCT = "product";
    private final static String PACKAGES = "packages";

    private LumpsumIfuaPresenter presenter;

    @State
    ProductList product;

    @State
    Packages packages;

    @Override
    protected int getLayout() {
        return R.layout.f_list_ifua;
    }

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragmentTransaction.replace(R.id.container, new LumspumIfuaFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    public static Fragment getFragment(ProductList product, Packages packages) {
        Fragment f = new LumspumIfuaFragment();
        Bundle extras = new Bundle();
        extras.putSerializable(PRODUCT, product);
        extras.putSerializable(PACKAGES, packages);
        f.setArguments(extras);
        return f;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        product  = (ProductList) getArguments().getSerializable(PRODUCT);
        packages = (Packages) getArguments().getSerializable(PACKAGES);
        presenter = new LumpsumIfuaPresenter(this);

                initRV();
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup(){
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 1 : 1;
            }
        });

        rv.setLayoutManager(layoutManager);
        loadCategoryPortfolio();
    }

    private void initRV() {
        LinearLayoutManager llManager = new LinearLayoutManager(getActivity());
        llManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llManager);
        rv.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                InvestmentAccountInfo accountInfo = lumpsum.get(position);
                presenter.getMaxScore(product, accountInfo);
            }
        }));
    }

    void noPortfolio(boolean b) {
        llNoPortfolio.setVisibility(b ? View.VISIBLE : View.GONE);
        llPortfolio.setVisibility(b ? View.GONE : View.VISIBLE);
    }


    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }


    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }


    //masih pake data dummy
    public void loadCategoryPortfolio(){
//        categoryPortfolios = new ArrayList<>();
//        CategoryPortfolio categoryPortfolio = new CategoryPortfolio();
//        categoryPortfolio.setCategoryId("12903");
//        categoryPortfolio.setCategoryName("Tabungan Haji");
//        categoryPortfolios.add(categoryPortfolio);
        rv.setAdapter(new CategoryPortfolioAdapter(getActivity(), lumpsum, packages));
    }



}
