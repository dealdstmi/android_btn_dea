package com.danareksa.investasik.ui.fragments.dashboard;

import android.widget.Toast;

import com.danareksa.investasik.data.api.beans.PortfolioChartData;
import com.danareksa.investasik.data.api.requests.InvestmentAcountGroupRequest;
import com.danareksa.investasik.data.api.responses.PortfolioInvestmentListResponse;
import com.danareksa.investasik.data.api.responses.PortfolioInvestmentSummaryResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by fajarfatur on 3/1/16.
 */
public class MyPortfolioPresenter {

    private MyPortfolioFragment fragment;

    public MyPortfolioPresenter(MyPortfolioFragment fragment) {
        this.fragment = fragment;
    }


    void getInvestmentList() {
        InvestmentAcountGroupRequest request = new InvestmentAcountGroupRequest();
        request.setToken(PrefHelper.getString(PrefKey.TOKEN));
        request.setInvAccGroupId(7); //TODO: Fix this GroupID

        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().getInvestmentList(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PortfolioInvestmentListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getMessage());
                        fragment.dismissProgressDialog();
                        fragment.noPortfolio(true);
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(PortfolioInvestmentListResponse portfolioInvestmentListResponse) {
                        fragment.dismissProgressDialog();
                        if (portfolioInvestmentListResponse.getCode() == 1) {
                            if (portfolioInvestmentListResponse.getData().getInvestmentList().size() > 0) {
                                fragment.noPortfolio(false);
                                getInvestmentSummary();
                                getPieChart();
                            } else {
                                fragment.noPortfolio(true);
                            }
                        }
                    }
                });
    }

    void getInvestmentSummary() {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().getInvestmentSummary(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PortfolioInvestmentSummaryResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getMessage());
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(PortfolioInvestmentSummaryResponse portfolioInvestmentSummaryResponse) {
                        fragment.dismissProgressDialog();
                        fragment.investmentSummary = portfolioInvestmentSummaryResponse;
                        fragment.loadInvestmentSummary();
                    }
                });
    }

    void getPieChart() {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().getPieChart(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<PortfolioChartData>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getMessage());
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(List<PortfolioChartData> portfolioChartDatas) {
                        fragment.dismissProgressDialog();
                        /*fragment.renderPieChart(portfolioChartDatas);*/

                    }
                });
    }

}
