package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.PromoResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.DetailPromoActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by asep.surahman on 03/04/2018.
 */

public class NewDashboardAdapter extends RecyclerView.Adapter<NewDashboardAdapter.NewDashboardHolder>{

    private Context context;
    private List<PromoResponse> list;

    public NewDashboardAdapter(Context context, List<PromoResponse> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public NewDashboardAdapter.NewDashboardHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_landing_page, parent, false);
        NewDashboardAdapter.NewDashboardHolder newDashboardHolder = new NewDashboardAdapter.NewDashboardHolder(itemView);
        return newDashboardHolder;
    }

    @Override
    public void onBindViewHolder(NewDashboardAdapter.NewDashboardHolder holder, int position){
        final PromoResponse item = list.get(position);
        holder.itemView.setTag(item);

        Glide.with(context).load(InviseeService.IMAGE_DOWNLOAD_URL + item.getImage())
                .placeholder(R.drawable.ic_logo_launcher)
                .error(R.drawable.ic_logo_launcher)
                .into(holder.ivLanding);

        holder.ivLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailPromoActivity.startActivity((BaseActivity) context, item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class NewDashboardHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ivLanding)
        ImageView ivLanding;

        public NewDashboardHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
