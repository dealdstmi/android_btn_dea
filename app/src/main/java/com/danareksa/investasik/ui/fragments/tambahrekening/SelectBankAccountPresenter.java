package com.danareksa.investasik.ui.fragments.tambahrekening;

import com.danareksa.investasik.data.api.requests.BankListRequest;
import com.danareksa.investasik.data.api.requests.CustomerDataRequest;
import com.danareksa.investasik.data.api.responses.CustomerDataRekeningResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 06/08/2018.
 */

public class SelectBankAccountPresenter {

    SelectBankAccountFragment fragment;

    public SelectBankAccountPresenter(SelectBankAccountFragment fragment){
        this.fragment = fragment;
    }

    public BankListRequest getToken(){
        BankListRequest bankListRequest = new BankListRequest();
        bankListRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        return bankListRequest;

    }


    public void getCustomerData(final String accountType){
        fragment.showProgressBar();
        fragment.getApi().getCustomerData(getCustomerDataRequest(accountType))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<CustomerDataRekeningResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(CustomerDataRekeningResponse customerDataRekeningResponse){
                        fragment.dismissProgressBar();
                        fragment.bankAccountList = customerDataRekeningResponse.getData().getBankAccount();
                        fragment.fetchResultToLayout();
                    }
                });
    }


    public CustomerDataRequest getCustomerDataRequest(String accountType){
        CustomerDataRequest customerDataRequest = new CustomerDataRequest();
        customerDataRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        customerDataRequest.setAccountType(accountType);
        return customerDataRequest;
    }


}
