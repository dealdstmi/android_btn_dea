package com.danareksa.investasik.ui.fragments.registernasabah;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.RegisterOnlineAccess;
import com.danareksa.investasik.data.api.requests.RegisterNasabahRequest;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.RegisterNasabahSyaratKetenActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 08/05/2018.
 */

public class RegisterNasabahlamaFragment extends BaseFragment {

    public static final String TAG = RegisterNasabahlamaFragment.class.getSimpleName();

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etKodeIfua)
    EditText etKodeIfua;

    @Email
    @NotEmpty(messageResId = R.string.rules_no_empty_email)
    @Bind(R.id.etEmail)
    EditText etEmail;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etNoIDKTP)
    EditText etNoIDKTP;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etNoHp)
    EditText etNoHP;

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new RegisterNasabahlamaFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    public static Fragment getFragment(){
        Fragment f = new RegisterNasabahlamaFragment();
        return f;
    }

    @Override
    protected int getLayout() {
        return R.layout.f_register_nasabah_lama_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onResume(){
        super.onResume();
    }


    public boolean validate(){

        boolean valid = true;
        String  kodeIfua  = etKodeIfua.getText().toString();
        String  email     = etEmail.getText().toString();
        String  nik     = etNoIDKTP.getText().toString();
        String  noHp     = etNoHP.getText().toString();

        if(BuildConfig.FLAVOR.contentEquals("btn")) {
            if(nik.isEmpty()){
                etNoIDKTP.setError("Mohon isi kolom ini");
                valid = false;
            }else{
                etNoIDKTP.setError(null);
            }

            if(noHp.isEmpty()){
                etNoHP.setError("Mohon isi kolom ini");
                valid = false;
            }else{
                etNoHP.setError(null);
            }

            if(email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                etEmail.setError("Masukkan email Anda dengan benar");
                valid = false;
            }
        } else if(BuildConfig.FLAVOR.contentEquals("pnm")) {

        } else {
            if(kodeIfua.isEmpty()){
                etKodeIfua.setError("Mohon isi kolom ini");
                valid = false;
            }else{
                etKodeIfua.setError(null);
            }

            if(email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                etEmail.setError("Masukkan email Anda dengan benar");
                valid = false;
            }
        }
        return valid;
    }


    public void onSignupFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }



    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
        if(BuildConfig.FLAVOR.contentEquals("btn")){
            RegisterNasabahSyaratKetenActivity.startActivity((BaseActivity) getActivity(), "lama", getRegisterNasabahRequest());
        } else if (BuildConfig.FLAVOR.contentEquals("pnm")){

        } else {
            RegisterNasabahSyaratKetenActivity.startActivity((BaseActivity) getActivity(), "lama", getRegisterOnlineAccessModel());
        }
    }


    @OnClick(R.id.bMulai)
    void bMulai(){

        if(etEmail.length() >0){
            String lowercase = etEmail.getText()+"";
            etEmail.setText(lowercase.toLowerCase());
        }

        if(!validate()){
            onSignupFailed();
            return;
        }else{
            validator.validate();
        }

    }

    private RegisterOnlineAccess getRegisterOnlineAccessModel(){
        RegisterOnlineAccess registerOnlineAccess = new RegisterOnlineAccess();
        registerOnlineAccess.setEmail(etEmail.getText().toString());
        registerOnlineAccess.setIfua(etKodeIfua.getText().toString());
        return registerOnlineAccess;
    }

    private RegisterNasabahRequest getRegisterNasabahRequest(){
        RegisterNasabahRequest registerNasabahRequest = new RegisterNasabahRequest();
        registerNasabahRequest.setEmail(etEmail.getText().toString());
        registerNasabahRequest.setMobilePhoneNo(etNoHP.getText().toString());
        registerNasabahRequest.setNik(etNoIDKTP.getText().toString());
        return registerNasabahRequest;
    }


    //cobain besok
    private boolean _hasLoadedOnce= false; // your boolean field
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);


        if (this.isVisible()){
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                //new NetCheck().execute(); metod
                System.out.println("=================>111111111111111");
                _hasLoadedOnce = true;
            }
        }
    }


}
