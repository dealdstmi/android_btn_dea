package com.danareksa.investasik.ui.adapters.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.ui.fragments.registernasabah.RegisterNasabahbaruFragment;
import com.danareksa.investasik.ui.fragments.registernasabah.RegisterNasabahlamaFragment;

/**
 * Created by asep.surahman on 11/05/2018.
 */

public class RegisterNasabahPageAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    private BaseFragment bFragment;
    private Packages packages;

    public RegisterNasabahPageAdapter(BaseFragment bFragment, FragmentManager fm) {
        super(fm);
        this.bFragment = bFragment;
        //this.packages = packages;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = RegisterNasabahbaruFragment.getFragment();
                break;
            case 1:
                fragment = RegisterNasabahlamaFragment.getFragment();
                break;
        }
        return fragment;
    }


}
