package com.danareksa.investasik.ui.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.danareksa.investasik.ui.fragments.cart.CartFragment;

import java.util.List;

import butterknife.Bind;

/**
 * Created by fajarfatur on 3/2/16.
 */
public class CartActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    private PortfolioInvestment investment;

    public static Activity fa;

    /*public static void startActivity(BaseActivity sourceActivity) {
        sourceActivity.startActivity(new Intent(sourceActivity, CartActivity.class));
    }*/

    String stringActivityCalling;

    public static void startActivity(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, CartActivity.class);
        intent.putExtra("classFrom", sourceActivity.getClass().toString());
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_cart;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fa = this;
        setSupportActionBar(toolbar);
        setTitle("");
        Bundle bundle = getIntent().getExtras();
        stringActivityCalling = bundle.getString("classFrom");

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title.setText("Pesanan Anda");

        CartFragment.showFragment(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        if(stringActivityCalling.contains("MainActivity")){
            startActivity(new Intent(CartActivity.this, MainActivity.class));
        }else if(stringActivityCalling.contains("UserProfileActivity")){
            startActivity(new Intent(CartActivity.this, UserProfileActivity.class));
        }else if(stringActivityCalling.contains("ListOfCatalogueActivity")){
            startActivity(new Intent(CartActivity.this, ListOfCatalogueActivity.class));
        }else if(stringActivityCalling.contains("CheckoutActivity")){
            startActivity(new Intent(CartActivity.this, MainActivity.class));
        }else if(stringActivityCalling.contains("ChooseIfuaActivity")){
            startActivity(new Intent(CartActivity.this, MainActivity.class));
        }else{
            System.out.println("start call : " + stringActivityCalling);
            finish();
        }
    }


    protected Boolean isActivityRunning(String canonicalNameClass)
    {
        ActivityManager activityManager = (ActivityManager) getBaseContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);
        for (ActivityManager.RunningTaskInfo task : tasks){
            System.out.println("activity running :: " + task.baseActivity.getClassName());
                if(canonicalNameClass.equalsIgnoreCase(task.baseActivity.getClassName())){
                    return true;
                }
        }
        return false;
    }


}
