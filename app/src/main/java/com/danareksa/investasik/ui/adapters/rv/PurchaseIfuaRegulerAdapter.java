package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.InvestmentAccountInfo;
import com.danareksa.investasik.data.api.beans.Packages;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by asep.surahman on 24/09/2018.
 */

public class PurchaseIfuaRegulerAdapter extends RecyclerView.Adapter<PurchaseIfuaRegulerAdapter.PurchaseIfuaRegulerHolder>{

    private Context context;
    Packages packages;
    private List<InvestmentAccountInfo> list = new ArrayList<>();
    private PurchaseIfuaRegulerAdapter.CallbackChekPackage mCallback;

    public PurchaseIfuaRegulerAdapter(Context context, List<InvestmentAccountInfo> list, Packages packages, PurchaseIfuaRegulerAdapter.CallbackChekPackage mCallbacks) {
        this.context = context;
        this.list = list;
        this.packages = packages;
        mCallback = mCallbacks;
    }

    @Override
    public PurchaseIfuaRegulerAdapter.PurchaseIfuaRegulerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_category_portfolio, parent, false);
        return new PurchaseIfuaRegulerAdapter.PurchaseIfuaRegulerHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PurchaseIfuaRegulerAdapter.PurchaseIfuaRegulerHolder holder, final int position) {
        final InvestmentAccountInfo item = list.get(position);
        holder.itemView.setTag(item);
        holder.tvCategoryId.setText(item.getIfua());
        holder.tvCategoryName.setText(item.getInvestmentGoal());
        holder.llCategoryPortfolio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                doCheckPackege(item.getIfua(), packages);
                //ListOfRegulerPurchaseActivity.startActivity((BaseActivity) context, item.getIfua(), packages);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class PurchaseIfuaRegulerHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvCategoryId)
        TextView tvCategoryId;
        @Bind(R.id.tvCategoryName)
        TextView tvCategoryName;
        @Bind(R.id.llCategoryPortfolio)
        LinearLayout llCategoryPortfolio;

        public PurchaseIfuaRegulerHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }



    public interface CallbackChekPackage{
        void checkPackage(String ifua,  Packages packages);
    }

    private void doCheckPackege(String ifua,  Packages packages){
        mCallback.checkPackage(ifua, packages);
    }

}
