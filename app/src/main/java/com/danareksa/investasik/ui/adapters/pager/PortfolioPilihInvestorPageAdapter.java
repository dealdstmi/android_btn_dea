package com.danareksa.investasik.ui.adapters.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.danareksa.investasik.data.api.beans.InvestmentAccountGroup;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.ui.fragments.portfolio.DetailCatPortfolioLumpsumFragment;
import com.danareksa.investasik.ui.fragments.portfolio.DetailCatPortfolioRegulerFragment;

/**
 * Created by asep.surahman on 21/05/2018.
 */

public class PortfolioPilihInvestorPageAdapter extends FragmentPagerAdapter{

    final int PAGE_COUNT = 2;
    private BaseFragment bFragment;
    private Packages packages;
    private InvestmentAccountGroup investmentAccountGroup;

    public PortfolioPilihInvestorPageAdapter(BaseFragment bFragment, FragmentManager fm) {
        super(fm);
        this.bFragment = bFragment;
    }

    public PortfolioPilihInvestorPageAdapter(BaseFragment bFragment, FragmentManager fm, InvestmentAccountGroup accountGroup) {
        super(fm);
        this.bFragment = bFragment;
        this.investmentAccountGroup = accountGroup;
    }

    @Override
    public int getCount(){
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = DetailCatPortfolioLumpsumFragment.getFragment(this.investmentAccountGroup.getLumpsum());
                ((DetailCatPortfolioLumpsumFragment) fragment).lumpsum = this.investmentAccountGroup.getLumpsum();
                break;
            case 1:
                fragment = DetailCatPortfolioRegulerFragment.getFragment(this.investmentAccountGroup.getReguler());
                ((DetailCatPortfolioRegulerFragment) fragment).regular = this.investmentAccountGroup.getReguler();
                break;
        }
        return fragment;
    }




}
