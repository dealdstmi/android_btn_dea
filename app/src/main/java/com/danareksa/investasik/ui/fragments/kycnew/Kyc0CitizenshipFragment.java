package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Country;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobsandgeeks.saripaar.Validator;

import java.lang.reflect.Type;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 15/07/2018.
 */

public class Kyc0CitizenshipFragment extends KycBaseNew{

    public static final String TAG = Kyc0CitizenshipFragment.class.getSimpleName();
    protected Validator validator;

    @Bind(R.id.tvIdNumber)
    TextView tvIdNumber;
    @Bind(R.id.tvIdTax)
    TextView tvIdTax;
    @Bind(R.id.etIdNumberKtp)
    EditText etIdNumberKtp;
    @Bind(R.id.etIdNumberPaspor)
    EditText etIdNumberPaspor;
    @Bind(R.id.etIdTax)
    EditText etIdTax;
    @Bind(R.id.sNationality)
    Spinner sNationality;
    @Bind(R.id.btWni)
    TextView btWni;
    @Bind(R.id.btWna)
    TextView btWna;
    @Bind(R.id.llCitizenship)
    LinearLayout llCitizenship;
    @Bind(R.id.tvName)
    TextView tvName;

    private String jsonCountry;
    public List<Country> countries;
    public KycDataRequest kycDataRequest;
    String citizenshipType = "";String name = "";

    public static void showFragment(BaseActivity sourceActivity){
        if (!sourceActivity.isFragmentNotNull(TAG)){
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc0CitizenshipFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    public static Fragment getFragment(KycDataRequest kycDataRequest, String jsonCountry){
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        bundle.putString("jsonCountry", jsonCountry);
        Kyc0CitizenshipFragment kyc0CitizenshipFragment = new Kyc0CitizenshipFragment();
        kyc0CitizenshipFragment.setArguments(bundle);
        return kyc0CitizenshipFragment;
    }


    @Override
    protected int getLayout(){
        return R.layout.f_register_citizenship;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        jsonCountry = getArguments().getString("jsonCountry");
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        init();
    }


    @Override
    public void onResume(){
        super.onResume();
    }


    public void init(){
        //tvName.setText("Hai, " + PrefHelper.getString(PrefKey.FIRST_NAME));
        try {
            name = request.getName();
            name = name.replace("  ", " ");
            tvName.setText("Hai, " + name);
        }catch (Exception e){
            e.printStackTrace();
        }

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            disableField();
        }

        if(request.getCitizenship() != null){
            if(!request.getCitizenship().equals("")){
                llCitizenship.setVisibility(View.VISIBLE);

                if(request.getCitizenship().equals(Constant.CITIZENSHIP_INDO)){

                    citizenshipType = Constant.CITIZENSHIP_INDO;
                    setViewFieldWni();
                    changeButtonActive(true, false);

                    if(request.getIdType() != null){
                        if(request.getIdType().equals(Constant.ID_TYPE_KTP)){
                            etIdNumberKtp.setText(request.getIdNumber());
                        }
                    }
                    if(request.getTaxId() != null){
                        etIdTax.setText(request.getTaxId());
                    }

                } else if(request.getCitizenship().equals(Constant.CITIZENSHIP_LUAR)){

                    citizenshipType = Constant.CITIZENSHIP_LUAR;
                    setViewFieldWna();
                    changeButtonActive(false, true);

                    if(request.getIdType() != null){
                        if(request.getIdType().equals(Constant.ID_TYPE_PAS)){
                            etIdNumberPaspor.setText(request.getIdNumber());
                        }
                    }
                }
            }

        }else{
            llCitizenship.setVisibility(View.VISIBLE);
            citizenshipType = Constant.CITIZENSHIP_INDO;
            changeButtonActive(true, false);
            setViewFieldWni();
        }


        Type listType = new TypeToken<List<Country>>(){}.getType();
        Gson gson = new Gson();
        countries = gson.fromJson(jsonCountry, listType);

        if(request.getNationality() != null){
            if(!request.getNationality().equals("")){
                setupSpinnerCountry(sNationality, countries, request.getNationality());
            }
        }else{
            setupSpinnerCountry(sNationality, countries, Constant.ID_COUNTRY_INDO);
        }
    }


    public void setValueCitizenship(){
        if(citizenshipType.equals(Constant.CITIZENSHIP_INDO)){
            request.setIdType(Constant.ID_TYPE_KTP);
            request.setCitizenship(Constant.CITIZENSHIP_INDO);
            request.setIdNumber(getAndTrimValueFromEditText(etIdNumberKtp));
            request.setTaxId(getAndTrimValueFromEditText(etIdTax));
            request.setNationality("52");
        }else if(citizenshipType.equals(Constant.CITIZENSHIP_LUAR)){
            request.setIdType(Constant.ID_TYPE_PAS);
            request.setCitizenship(Constant.CITIZENSHIP_LUAR);
            request.setIdNumber(getAndTrimValueFromEditText(etIdNumberPaspor));
            request.setTaxId("");
            request.setNationality(String.valueOf(countries.get(sNationality.getSelectedItemPosition()).getId()));
        }
    }


    @Override
    public void saveDataKycWithBackpress(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
                setValueCitizenship();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }


    @Override
    public void previewsWithoutValidation() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
    }


    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }else{
            if(!validate()){
                onValidFailed();
            }else{
                setValueCitizenship();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }
            return;
        }

    }


    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }



    @OnClick(R.id.llNewCustomer)
    void llNewCustomer(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            llCitizenship.setVisibility(View.VISIBLE);
            citizenshipType = Constant.CITIZENSHIP_INDO;
            changeButtonActive(true, false);
            setViewFieldWni();
            setValueCitizenship();
        }
    }


    @OnClick(R.id.llOldCustomer)
    void llOldCustomer(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            llCitizenship.setVisibility(View.VISIBLE);
            citizenshipType = Constant.CITIZENSHIP_LUAR;
            changeButtonActive(false, true);
            setViewFieldWna();
            setValueCitizenship();
        }
    }



    public boolean validate(){
        boolean valid = true;
        String idNumberKtp = etIdNumberKtp.getText().toString();
        String idNumberPas = etIdNumberPaspor.getText().toString();

        if(citizenshipType.equals("")){
            Toast.makeText(getActivity(), "Pilih Kewarganegaraan", Toast.LENGTH_SHORT).show();
            valid = false;
        }

        if(citizenshipType.equals(Constant.CITIZENSHIP_INDO)){
            if(idNumberKtp.isEmpty() && idNumberKtp.length() <= 0){
                etIdNumberKtp.setError("Mohon isi kolom ini");
                valid = false;
            }else{
                etIdNumberKtp.setError(null);
            }

            if(idNumberKtp.length() < 16 || idNumberKtp.length() > 16){
                etIdNumberKtp.setError("Data tidak sesuai");
                valid = false;
            }
        }


        if(citizenshipType.equals(Constant.CITIZENSHIP_LUAR)){

            if(sNationality.getSelectedItem().toString().equals("")){
                ((TextView) sNationality.getSelectedView()).setError("Mohon isi kolom ini");
                valid = false;
            }

            if(idNumberPas.isEmpty() && idNumberPas.length() <= 0){
                etIdNumberPaspor.setError("Mohon isi kolom ini");
                valid = false;
            }

            if(!idNumberPas.isEmpty()){
                if(idNumberPas.length() < 3){
                    etIdNumberPaspor.setError("Data tidak sesuai");
                    valid = false;
                }
            }

        }

        return valid;
    }


    private void setViewFieldWni(){
        etIdTax.setVisibility(View.VISIBLE);
        sNationality.setVisibility(View.GONE);
        etIdNumberPaspor.setVisibility(View.GONE);
        etIdNumberKtp.setVisibility(View.VISIBLE);
        tvIdNumber.setText("KTP");
        tvIdTax.setText("NPWP (jika ada)");
        btWna.setText("B");
        btWni.setText(getCheckedIcon());
    }

    private void setViewFieldWna(){
        tvIdNumber.setText("Paspor");
        tvIdTax.setText("Sebutkan nama negara");
        etIdTax.setVisibility(View.GONE);
        etIdNumberKtp.setVisibility(View.GONE);
        etIdNumberPaspor.setVisibility(View.VISIBLE);
        sNationality.setVisibility(View.VISIBLE);
        btWni.setText("A");
        btWna.setText(getCheckedIcon());
    }


    public void disableField(){
        etIdNumberKtp.setEnabled(false);
        etIdNumberPaspor.setEnabled(false);
        sNationality.setEnabled(false);
        etIdTax.setEnabled(false);
    }


    private void changeButtonActive(boolean btWnin, boolean btWnas){
        btWni.setEnabled(btWnin);
        btWna.setEnabled(btWnas);
    }

    private Spannable getCheckedIcon(){
        Spannable buttonLabel = new SpannableString(" ");
        buttonLabel.setSpan(new ImageSpan(getActivity().getApplicationContext(), R.drawable.ic_checked,
                ImageSpan.ALIGN_BOTTOM), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return  buttonLabel;
    }


    @Override
    public void previewsWhileError(){

    }


}
