package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.fragments.termsandcondition.TermsAndConditionFragment;

import butterknife.Bind;

/**
 * Created by pandu.abbiyuarsyah on 18/04/2017.
 */

public class TermsAndConditionActivity extends BaseActivity {


    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    public static void startActivity(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, TermsAndConditionActivity.class);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_fatca;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        setTitle("");

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText("Syarat dan ketentuan");
        TermsAndConditionFragment.showFragment(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
//        startActivity(new Intent(TermsAndConditionActivity.this, MainActivity.class));
//        finish();

        if (PrefHelper.getBoolean(PrefKey.IS_LOGIN)) {
            startActivity(new Intent(TermsAndConditionActivity.this, MainActivity.class));
        } else {
            startActivity(new Intent(TermsAndConditionActivity.this, LandingPageActivity.class));
        }
        finish();
    }


}
