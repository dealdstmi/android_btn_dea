package com.danareksa.investasik.ui.fragments.dashboard;

import com.danareksa.investasik.data.api.beans.PieChartDataDashboard;
import com.danareksa.investasik.data.api.requests.InvestmentAcountGroupRequest;
import com.danareksa.investasik.data.api.requests.PieChartRequest;
import com.danareksa.investasik.data.api.responses.PieChartResponse;
import com.danareksa.investasik.data.api.responses.PortfolioInvestmentListResponse;
import com.danareksa.investasik.data.api.responses.PortfolioInvestmentSummaryResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ivan.pradana on 3/8/2017.
 */

public class DetailDashboardPresenter {


    private DetailDashboardFragment fragment;

    public DetailDashboardPresenter(DetailDashboardFragment fragment){
        this.fragment = fragment;
    }

    void investmentList() {

        InvestmentAcountGroupRequest request = new InvestmentAcountGroupRequest();
        request.setToken(PrefHelper.getString(PrefKey.TOKEN));
        request.setInvAccGroupId(7); //TODO: Fix this GroupID

        fragment.showProgressBar();
        fragment.getApi().getInvestmentList(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PortfolioInvestmentListResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(PortfolioInvestmentListResponse portfolioInvestmentListResponse) {
                        if (portfolioInvestmentListResponse.getCode() == 1) {
                            if (portfolioInvestmentListResponse.getData().getInvestmentList().size() > 0) {
                                fragment.noPortfolio(false);
                                getInvestmentSummary();
                            } else {
                                fragment.noPortfolio(true);
                                fragment.dismissProgressBar();
                            }
                        }


                    }
                });
    }


    void getInvestmentSummary() {
        fragment.showProgressBar();
        fragment.getApi().getInvestmentSummary(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PortfolioInvestmentSummaryResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(PortfolioInvestmentSummaryResponse portfolioInvestmentSummaryResponse) {

                        fragment.dismissProgressBar();
                        if (portfolioInvestmentSummaryResponse.getCode() == 1) {
                            if (portfolioInvestmentSummaryResponse.getTotalMarketValue() > 0) {
                                fragment.noPortfolio(false);
                                fragment.investmentSummary = portfolioInvestmentSummaryResponse;
                                fragment.loadInvestmentSummary();
                                getPieChartDashboard();
                            } else {
                                fragment.noPortfolio(true);
                                fragment.dismissProgressBar();
                            }
                        }
                    }
                });
    }


    PieChartRequest constructPieChartRequest() {
        PieChartRequest pieChartRequest = new PieChartRequest();
        pieChartRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        return pieChartRequest;

    }

    void getPieChartDashboard() {
        fragment.getApi().getPieChartDashboard(PrefHelper.getString(PrefKey.TOKEN), constructPieChartRequest())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PieChartResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(PieChartResponse pieChartResponse) {
                        List<PieChartDataDashboard> pieChartDataDashboards = pieChartResponse.getData().getListDetail();
                        fragment.renderPieChartDashboard(pieChartDataDashboards);
                        fragment.initRV();
                        fragment.dismissProgressBar();

                    }
                });
    }


}
