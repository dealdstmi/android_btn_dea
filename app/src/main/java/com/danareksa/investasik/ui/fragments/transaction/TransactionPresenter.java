package com.danareksa.investasik.ui.fragments.transaction;

import android.view.View;

import com.danareksa.investasik.data.api.beans.TransactionHistory;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by fajarfatur on 3/21/16.
 */
public class TransactionPresenter {

    private static final String MAX = "500";

    private TransactionFragment fragment;

    public TransactionPresenter(TransactionFragment fragment) {
        this.fragment = fragment;
    }

    void getTransactionHistory(String name) {
        fragment.showProgressBar();
        fragment.getApi().getTransactionHistory(PrefHelper.getString(PrefKey.TOKEN), MAX, name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<TransactionHistory>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();

                    }

                    @Override
                    public void onNext(List<TransactionHistory> transactionHistories) {
                        fragment.transactionHistories = (ArrayList<TransactionHistory>) transactionHistories;
                        if (transactionHistories.size() > 0) {
                            fragment.sTrxName.setVisibility(View.VISIBLE);
                            fragment.loadTrxHistoryList();
                            fragment.dismissProgressBar();
                        } else {
                            fragment.tvMessage.setVisibility(View.VISIBLE);
                            fragment.rv.setVisibility(View.GONE);
                            fragment.line.setVisibility(View.GONE);
                            fragment.dismissProgressBar();
                        }

                    }
                });
    }
}
