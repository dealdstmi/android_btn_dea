package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;

import butterknife.Bind;

/**
 * Created by asep.surahman on 21/05/2018.
 */

public class Kyc12AdvancedHeirFragment extends KycBaseNew {

    public static final String TAG = Kyc12AdvancedHeirFragment.class.getSimpleName();
    public static final String KYC_DATA_REQUEST = "kycDataRequest";

    @Bind(R.id.etHeirName)
    EditText etHeirName;

    @Bind(R.id.etHeirMobileNumber)
    EditText etHeirMobileNumber;

    @Bind(R.id.sHeirRelationship)
    Spinner sHeirRelationship;

    @Bind(R.id.sHeirInvestmentGoal)
    Spinner sHeirInvestmentGoal;

    @Bind(R.id.etHeirRelationshipOther)
    EditText etHeirRelationshipOther;
    String heirRelationshipOther = "";


    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc12AdvancedHeirFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    public static Fragment getFragment(KycDataRequest kycDataRequest){
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        Kyc12AdvancedHeirFragment fragment = new Kyc12AdvancedHeirFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayout(){
        return R.layout.f_register_lanjut_ahli_waris;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onResume(){
        super.onResume();
    }


    public void init(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            disableField();
        }

        if(request.getHeirRelationship() != null){
            if(request.getHeirRelationship().equalsIgnoreCase("oth")){
                showFieldOtherRelationship();
                etHeirRelationshipOther.setText(request.getHeirRelationshipOther());
                heirRelationshipOther = request.getHeirRelationshipOther();
            }else{
                hideFieldOtherRelationship();
            }
        }else{
            heirRelationshipOther = "";
            hideFieldOtherRelationship();
        }

        etHeirName.setText(request.getHeirName());
        etHeirMobileNumber.setText(request.getHeirMobileNumber());
        setupSpinnerWithSpecificLookupSelection(sHeirInvestmentGoal, getKycLookupFromRealm(Constant.KYC_CAT_INVESTMENT_GOAL), request.getHeirInvestmentGoal());
        setupSpinnerWithSpecificLookupSelection(sHeirRelationship, getKycLookupFromRealm(Constant.KYC_CAT_RELATIONSHIP_HEIR), request.getHeirRelationship());
        onItemSelectedItem();
    }

    public void setValueHeir(){
        request.setHeirName(getAndTrimValueFromEditText(etHeirName));
        request.setHeirMobileNumber(getAndTrimValueFromEditText(etHeirMobileNumber));
        request.setHeirRelationship(getKycLookupCodeFromSelectedItemSpinner(sHeirRelationship));
        request.setHeirRelationshipOther(getAndTrimValueFromEditText(etHeirRelationshipOther));
        request.setHeirInvestmentGoal(getKycLookupCodeFromSelectedItemSpinner(sHeirInvestmentGoal));
    }


    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }else{
            if(!validate()){
                onValidFailed();
            }else{
                setValueHeir();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }
            return;
        }

    }

    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    @Override
    public void saveDataKycWithBackpress(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
                setValueHeir();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }


    @Override
    public void previewsWithoutValidation() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }


    //disable
    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
    }


    public void onItemSelectedItem(){
        sHeirRelationship.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String itemRelationship = String.valueOf(adapterView.getItemAtPosition(i).toString());
                String otherRelationship = itemRelationship.toLowerCase();
                if(otherRelationship.contains("lainnya") || otherRelationship.contains("others")){
                    showFieldOtherRelationship();
                    heirRelationshipOther = getAndTrimValueFromEditText(etHeirRelationshipOther);
                }else{
                    hideFieldOtherRelationship();
                    heirRelationshipOther = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView){

            }
        });
    }


    private void showFieldOtherRelationship(){
        etHeirRelationshipOther.setVisibility(View.VISIBLE);
    }

    private void hideFieldOtherRelationship(){
        etHeirRelationshipOther.setVisibility(View.GONE);
    }

    private void disableField(){
        etHeirName.setEnabled(false);
        etHeirMobileNumber.setEnabled(false);
        etHeirRelationshipOther.setEnabled(false);
        sHeirInvestmentGoal.setEnabled(false);
        sHeirRelationship.setEnabled(false);
    }


    public boolean validate(){
        boolean valid = true;
        String heirName              = etHeirName.getText().toString();
        String heirMobileNumber      = etHeirMobileNumber.getText().toString();
        String relationshipOther     = etHeirRelationshipOther.getText().toString();

        if(heirName.isEmpty() && heirName.length() <= 0){
            etHeirName.setError("Mohon isi kolom ini");
            valid = false;
        }

        if(!heirName.isEmpty() && heirName.length() < 3){
            etHeirName.setError("Mohon isi nama ahli waris dengan benar");
            valid = false;
        }

        if(heirMobileNumber.isEmpty() && heirMobileNumber.length() <= 0){
            etHeirMobileNumber.setError("Mohon isi kolom ini");
            valid = false;
        }

        if(!heirMobileNumber.isEmpty() && heirMobileNumber.length() < 3){
            valid = false;
            etHeirMobileNumber.setError("Nomor telepon tidak sesuai");
        }

        if(sHeirRelationship.getSelectedItem().toString().equalsIgnoreCase("lainnya") || sHeirRelationship.getSelectedItem().toString().equalsIgnoreCase("others")){
            if(!etHeirRelationshipOther.equals("")){
                if(relationshipOther.isEmpty() && relationshipOther.length() <= 0){
                    etHeirRelationshipOther.setError("Mohon isi kolom ini");
                    valid = false;
                }

                if(!relationshipOther.isEmpty() && relationshipOther.length() < 3){
                    etHeirRelationshipOther.setError("Mohon isi ahli waris dengan benar");
                    valid = false;
                }
            }
        }

//        if(sHeirInvestmentGoal.getSelectedItem().toString().equals("")){
//            valid = false;
//            ((TextView) sHeirInvestmentGoal.getSelectedView()).setError("Mohon isi kolom ini");
//        }

        if(sHeirRelationship.getSelectedItem().toString().equals("") ||
            sHeirRelationship.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sHeirRelationship.getSelectedView()).setError("Mohon isi kolom ini");
        }

        return  valid;
    }



    /*
        "heirName" : "Asiyah Putri",
        "heirMobileNumber" : "62-85736323622",
        "heirRelationship" : "DTR",
        "heirRelationshipOther" : "NO",
        "heirInvestmentGoal":""
    */


    @Override
    public void previewsWhileError(){

    }


}
