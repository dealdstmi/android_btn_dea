package com.danareksa.investasik.ui.fragments.tambahrekening;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Bank;
import com.danareksa.investasik.data.api.beans.BankAccount;
import com.danareksa.investasik.data.api.beans.Branch;
import com.danareksa.investasik.data.api.beans.Country;
import com.danareksa.investasik.data.api.beans.KycLookup;
import com.danareksa.investasik.data.api.requests.AddAccountRequest;
import com.danareksa.investasik.data.realm.RealmHelper;
import com.danareksa.investasik.data.realm.model.KycLookupModel;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.eventBus.RxBusObject;

import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import icepick.State;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by asep.surahman on 25/07/2018.
 */

public abstract class BaseInvest  extends BaseFragment{

    public static final String ADD_ACCOUNT_REQUEST = "accountRequest";
    public int defaultIndexCountry = 0;
    private Country country;
    public String valueEditText;

    protected boolean visibleToUser = false;

    @State
    AddAccountRequest acountRequest;

    public Realm investRealm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.investRealm = Realm.getDefaultInstance();
        if(getArguments() != null && getArguments().get(ADD_ACCOUNT_REQUEST) != null){
            acountRequest = (AddAccountRequest) getArguments().getSerializable(ADD_ACCOUNT_REQUEST);
        }else{
            acountRequest = new AddAccountRequest();
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);
        this.visibleToUser = isVisibleToUser;
    }


    @Override
    public void busHandler(RxBusObject.RxBusKey busKey, Object busObject){
        super.busHandler(busKey, busObject);
        switch (busKey){
            case GET_NEW_INPUTTED_INVEST:
                if(visibleToUser){
                    nextWithoutValidation();
                }
                break;
            case BACK_TAMBAH_REKENING:
                if(visibleToUser){
                    addAccountBackpress();
                }
                break;
        }
    }



    public Realm getRealmInvest(){
        return investRealm;
    }


    protected List<KycLookup>
    getKycLookupFromRealm(String category){
        realm = Realm.getDefaultInstance();
        List<KycLookup> list = new ArrayList<>();
        ModelMapper modelMapperDefaultValue = new ModelMapper();
        KycLookupModel kycLookupModelDefaultValue = new KycLookupModel();
        kycLookupModelDefaultValue.setCategory(category);
        kycLookupModelDefaultValue.setCode("");
        kycLookupModelDefaultValue.setValue("");
        kycLookupModelDefaultValue.setSeq(0); //new
        KycLookup dataFromRealm2 = modelMapperDefaultValue.map(kycLookupModelDefaultValue, KycLookup.class);
        list.add(dataFromRealm2);
        RealmResults<KycLookupModel> kycLookupModels = RealmHelper.readKycLookupListByCategory(realm, category);
        if(kycLookupModels != null){
            for (KycLookupModel model : kycLookupModels){
                ModelMapper modelMapper = new ModelMapper();
                KycLookup dataFromRealm = modelMapper.map(model, KycLookup.class); // map to non realm object
                list.add(dataFromRealm);
            }
        }
        return list;
    }

    protected String getKycLookupCodeFromSelectedItemSpinner(Spinner s) {
        return ((KycLookup) s.getSelectedItem()).getCode();
    }

    protected void setupSpinner(Spinner s, List<KycLookup> kycLookupList) {
        ArrayAdapter<KycLookup> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner, kycLookupList);
        s.setAdapter(spinnerArrayAdapter);
        s.setSelection(0, false);
    }

    protected void setupSpinnerWithSpecificLookupSelection(Spinner s, List<KycLookup> kycLookupList, String lookupCode) {
        setupSpinner(s, kycLookupList);
        int i = 0;
        for (KycLookup lookup : kycLookupList) {
            if (lookup.getCode().equalsIgnoreCase(lookupCode)) {
                s.setSelection(i, false);
                break;
            }
            i++;
        }
    }

    protected String getAndTrimValueFromEditText(EditText e){
        return e.getText().toString().trim();
    }

    protected String constructPhoneNumber(EditText countryCode, EditText cityCode, EditText phoneNumber) {
        StringBuilder constructedPhoneNumber = new StringBuilder("");
        constructedPhoneNumber
                .append(getAndTrimValueFromEditText(countryCode))
                .append("-")
                .append(getAndTrimValueFromEditText(cityCode))
                .append("-")
                .append(getAndTrimValueFromEditText(phoneNumber));
        return constructedPhoneNumber.toString();
    }



    public void setupSpinnerCountry(Spinner s, List<Country> countries, String selectionCountryId) {
        if (countries == null) countries = new ArrayList<>();

        Country defaultCountry = new Country();
        defaultCountry.setCountryName("");
        defaultCountry.setId(0);
        defaultCountry.setNumericCode("");
        defaultCountry.setAtCountryCode("");
        defaultCountry.setAlpha3Code("");
        countries.add(0, defaultCountry);

        ArrayAdapter<Country> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner, countries);
        s.setAdapter(spinnerArrayAdapter);
        s.setSelection(defaultIndexCountry, false); // set to default (Indonesia)

        if (!selectionCountryId.equalsIgnoreCase("null")) {
            int i = 0;
            for (Country country : countries) {
                if (country.getId() == Integer.valueOf(selectionCountryId)) {
                    s.setSelection(i, false);
                    this.country = country;
                    break;
                }
                i++;
            }
        }
    }



    public void setupSpinnerBank(Spinner s, List<Bank> banks, String selectionBankId){
        if (banks == null) banks = new ArrayList<>();

        Bank defaultBank = new Bank();
        defaultBank.setId("");
        defaultBank.setName("");
        banks.add(0, defaultBank);

        ArrayAdapter<Bank> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner, banks);
        s.setAdapter(spinnerArrayAdapter);
        s.setSelection(0, false);
        if (!selectionBankId.equalsIgnoreCase("null")) { // set selection to saved data from realm / ws
            int i = 0;
            for (Bank bank : banks) {
                if (bank.getId().equalsIgnoreCase(selectionBankId)) {
                    s.setSelection(i, false);
                    break;
                }
                i++;
            }
        }
    }


    public void setupSpinnerBranch(Spinner s, List<Branch> branches, String selectionBrachId){
        int selectedBranch;
        if(branches == null) branches = new ArrayList<>();

        Branch branch = new Branch();
        branch.setId(0);
        branch.setBranchName("");
        branches.add(0, branch);
        ArrayAdapter<Branch> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner, branches);
        s.setAdapter(spinnerArrayAdapter);
        s.setSelection(0, false);
        if (!selectionBrachId.equalsIgnoreCase("null")){ // set selection to saved data from realm / ws
            int i = 0;
            for(Branch branch1 : branches){
                if(!selectionBrachId.equals("")){
                    selectedBranch = Integer.valueOf(selectionBrachId);
                    if(branch1.getId() == selectedBranch){
                        s.setSelection(i, false);
                        break;
                    }
                    i++;
                }
            }
        }
    }


    public void setupSpinnerBankAccount(Spinner s, List<BankAccount> bankAccount, String selectionBankAccId){

        if (bankAccount == null) bankAccount = new ArrayList<>();
        BankAccount defaultBank = new BankAccount();
        defaultBank.setBankAccountId(0);
        defaultBank.setBankName("");
        defaultBank.setBankAccountNumber("");
        bankAccount.add(0, defaultBank);

        ArrayAdapter<BankAccount> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner, bankAccount);
        s.setAdapter(spinnerArrayAdapter);
        s.setSelection(0, false);
    }




    public String getText (final EditText e) {

        e.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus == false){
                    valueEditText = e.getText().toString();
                }

            }
        });
        return valueEditText;
    }


    public abstract void nextWithoutValidation();
    public abstract void saveDataKycWithBackpress();
    public abstract void addAccountBackpress();


}
