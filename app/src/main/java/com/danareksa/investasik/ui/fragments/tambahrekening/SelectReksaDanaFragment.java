package com.danareksa.investasik.ui.fragments.tambahrekening;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.KycLookup;
import com.danareksa.investasik.data.api.beans.PackageList;
import com.danareksa.investasik.data.api.beans.PackageListProduct;
import com.danareksa.investasik.data.api.beans.PackageListReguler;
import com.danareksa.investasik.data.api.requests.AddAccountRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.adapters.rv.SelectReksaDanaListAdapter;
import com.danareksa.investasik.util.AmountFormatter;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by asep.surahman on 25/07/2018.
 */


@RuntimePermissions
public class SelectReksaDanaFragment extends BaseInvest implements SelectReksaDanaListAdapter.CallbackPackage {

    public static final String TAG = SelectReksaDanaFragment.class.getSimpleName();
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.rv)
    RecyclerView rv;
    @Bind(R.id.btnAddPackage)
    Button btnAddPackage;
    String typeAccount;
    String jsonCountry;
    String jsonBank;

    List<KycLookup> kycLookupTimePeriod;
    SelectReksaDanaPresenter presenter;
    public List<PackageListReguler> packageListRegulers;
    public List<PackageListReguler> packageListRegulersAll;
    SelectReksaDanaListAdapter adapter;
    public static final String ADD_ACCOUNT_REQUEST = "accountRequest";


    public static Fragment getFragment(String jsonCountry, String jsonBank, String typeAccount,  AddAccountRequest acountRequest){
        Bundle bundle = new Bundle();
        bundle.putString("jsonCountry", jsonCountry);
        bundle.putString("jsonBank", jsonBank);
        bundle.putString("type", typeAccount);
        bundle.putSerializable(ADD_ACCOUNT_REQUEST, acountRequest);
        SelectReksaDanaFragment fragment = new SelectReksaDanaFragment();
        fragment.setArguments(bundle);
        return fragment;
    }



    @Override
    protected int getLayout(){
        return R.layout.f_tambah_rekening_pilih_reksa_dana;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        typeAccount = getArguments().getString("type");
        jsonCountry = getArguments().getString("jsonCountry");
        jsonBank = getArguments().getString("jsonBank");
        packageListRegulers = new ArrayList<>();
        packageListRegulersAll = new ArrayList<>();
        presenter = new SelectReksaDanaPresenter(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        init();
        presenter.getPackageList();
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
    }


    @Override
    public void onResume(){
        super.onResume();
    }

    private void init(){
        kycLookupTimePeriod = getKycLookupFromRealm(Constant.KYC_CAT_REGULER_PERIOD);
    }

    public void loadFirstRole(){
        if(packageListRegulers != null && packageListRegulers.size() != 0){
            addFirstListPackages();
        }else{
            addFirstListPackages();
        }
        loadList();
    }


    public void loadRole(){
        if(packageListRegulers != null && packageListRegulers.size() != 0){
            updateListPackage();
            addListPackage();
        }else{
            addFirstListPackages();
        }
        loadList();
    }



    private void updateListPackage(){
        if(adapter.getList() != null && adapter.getList().size() != 0){
            packageListRegulers = new ArrayList<>();
            packageListRegulers = presenter.gePackageListRegulers(adapter.getList());
        }
    }

    private void addFirstListPackages(){
        packageListRegulers = new ArrayList<>();
        PackageListReguler packageListReguler = new PackageListReguler();
        packageListReguler.setId(0);
        packageListReguler.setFeePercentage("");
        packageListReguler.setFeePrice(0.0);
        packageListReguler.setTransactionAmount("");
        packageListReguler.setTotal("");
        packageListReguler.setCode("");
        packageListReguler.setName("");
        packageListReguler.setTimePeriod("");
        packageListReguler.setMinSubscription(0.0);
        packageListReguler.setAccept(false);
        packageListRegulers.add(packageListReguler);
    }

    private void addListPackage(){
        PackageListReguler packageListReguler = new PackageListReguler();
        packageListReguler.setId(0);
        packageListReguler.setFeePercentage("");
        packageListReguler.setFeePrice(0.0);
        packageListReguler.setTransactionAmount("");
        packageListReguler.setTotal("");
        packageListReguler.setCode("");
        packageListReguler.setName("");
        packageListReguler.setTimePeriod("");
        packageListReguler.setMinSubscription(0.0);
        packageListReguler.setAccept(false);
        packageListRegulers.add(packageListReguler);
    }

    private void loadList(){
        adapter = new SelectReksaDanaListAdapter(presenter, packageListRegulers, packageListRegulersAll, getActivity(), this, kycLookupTimePeriod);
        rv.setAdapter(adapter);
    }


    @OnClick(R.id.btnAddPackage)
    void btnAddPackage(){
        loadRole();
    }


    @OnClick(R.id.btnNext)
    void btnNext(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            setValueReksaDana();
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }
    }


    private void setValueReksaDana(){
        acountRequest.setPackageList(getValueReksaDana().getPackageLists());
    }


    private PackageListProduct getValueReksaDana(){
        PackageListProduct packageListProduct = new PackageListProduct();
        List<PackageList> packageLists = new ArrayList<>();
        if(adapter.getList() != null && adapter.getList().size() != 0){
            for(int i = 0; i < adapter.getList().size(); i++){
                PackageList packageList = new PackageList();
                packageList.setMonthlyInvestmentAmount(adapter.getList().get(i).getTransactionAmount());
                packageList.setPackageId(String.valueOf(adapter.getList().get(i).getId()));
                packageList.setRegulerPeriod(adapter.getList().get(i).getTimePeriod());
                packageLists.add(packageList);
            }
        }
        packageListProduct.setPackageLists(packageLists);
        return packageListProduct;
    }

    boolean isDuplicates(final List<PackageListReguler> packageListRegulers)
    {
        List<Integer> packages = new ArrayList<>();
        for (PackageListReguler i : packageListRegulers)
        {
            if (packages.contains(i.getId())) return true;
            packages.add(i.getId());
        }
        return false;
    }

    public boolean validate(){
        boolean valid = true;
        if(adapter.getList() != null && adapter.getList().size() != 0){
            for(int i = 0; i < adapter.getList().size(); i++){

                System.out.println("min trans : " + adapter.getList().get(i).getMinSubscription());
                if(!adapter.getList().get(i).getTransactionAmount().equals("")){
                    double amount = Double.parseDouble(adapter.getList().get(i).getTransactionAmount());
                    //if(amount < adapter.getList().get(i).getMinSubscription()){
                    if(amount < 200000){
                        valid = false;
                        showFailedDialog("Minimal pembelian untuk " + adapter.getList().get(i).getName() +
                                " adalah " +
                                AmountFormatter.formatIdrWithoutComma(adapter.getList().get(i).getMinSubscription()));
                        break;
                    }else if(adapter.getList().get(i).getTransactionAmount().equals("0")){
                        valid = false;
                        showFailedDialog("Minimal pembelian untuk " + adapter.getList().get(i).getName() +
                                " adalah " +
                                AmountFormatter.formatIdrWithoutComma(adapter.getList().get(i).getMinSubscription()));
                        break;
                    }
                }else{
                    valid = false;
                    Toast.makeText(getContext(), "Nilai investasi harus diisi!", Toast.LENGTH_LONG).show();
                    break;
                }

                if(adapter.getList().get(i).getTimePeriod().equals("")){
                    valid = false;
                    Toast.makeText(getContext(), "Jangka waktu tidak boleh kosong!", Toast.LENGTH_LONG).show();
                    break;
                }

                if(adapter.getList().get(i).getId() == 0){
                    valid = false;
                    Toast.makeText(getContext(), "Silahkah pilih package!", Toast.LENGTH_LONG).show();
                    break;
                }

                if(adapter.getList().get(i).isAccept() == false){
                    valid = false;
                    Toast.makeText(getContext(), "Anda belum menyetujui produk!", Toast.LENGTH_LONG).show();
                    break;
                }

            }

            if (isDuplicates(adapter.getList())){
                showFailedDialog("Anda tidak dapat memilih produk yang sama. Silakan pilih produk lainnya.");
                valid = false;
            }

        }else{
            valid = false;
            Toast.makeText(getContext(), "Data tidak boleh kosong!", Toast.LENGTH_LONG).show();
        }
        return  valid;
    }



    @Override
    public void removePackage(int index){
        if(adapter.getList() != null && adapter.getList().size() != 0){
            packageListRegulers = new ArrayList<>();
            packageListRegulers = presenter.gePackageListRegulers(adapter.getList());
            packageListRegulers.remove(index);
            loadList();
        }
    }

    @Override
    public void downloadProspektus(String prospektusKey, String packageName){
        SelectReksaDanaFragmentPermissionsDispatcher.downloadProspectusWithPermissionCheck(SelectReksaDanaFragment.this, prospektusKey, packageName);
    }


    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void downloadProspectus(String prospektusKey, String packageName){
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(InviseeService.IMAGE_DOWNLOAD_URL + prospektusKey.toString() + "&token=" + PrefHelper.getString(PrefKey.TOKEN)));
        request.setDescription("A download package with some files");
        request.setTitle("Prospektus "+ packageName);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.allowScanningByMediaScanner();
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Prospektus "+packageName +".pdf");
        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }



    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar() {
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    @Override
    public void nextWithoutValidation(){
    }

    @Override
    public void saveDataKycWithBackpress(){

    }

    @Override
    public void addAccountBackpress() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }


}
