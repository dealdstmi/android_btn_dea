package com.danareksa.investasik.ui.adapters.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.ui.fragments.catalogue.BusinessRuleFragment;
import com.danareksa.investasik.ui.fragments.catalogue.PackagePerformanceFragment;

import java.util.ArrayList;
import java.util.List;

public class DetailOfCataloguePagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    private BaseFragment bFragment;
    private Packages packages;
    private final List<String> mFragmentTitles = new ArrayList<>();

    public DetailOfCataloguePagerAdapter(BaseFragment bFragment, FragmentManager fm, Packages packages) {
        super(fm);
        this.bFragment = bFragment;
        this.packages = packages;
    }

    public void addFragmentTitle(String title){
        mFragmentTitles.add(title);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position){
        Fragment fragment = null;
        switch (position) {
            /*case 0:
                fragment = FundAllocationFragment.getFragment(packages);
                break;*/
            case 0:
                fragment = PackagePerformanceFragment.getFragment(packages);
                break;
            case 1:
                fragment = BusinessRuleFragment.getFragment(packages);
                break;
        }

        return fragment;
    }


    @Override
    public CharSequence getPageTitle(int position){
        return mFragmentTitles.get(position);
    }

}

