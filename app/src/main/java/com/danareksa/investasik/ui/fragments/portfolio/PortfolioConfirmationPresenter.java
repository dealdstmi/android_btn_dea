package com.danareksa.investasik.ui.fragments.portfolio;

import android.graphics.Color;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Fee;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.FundAllocationResponse;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.CartActivity;
import com.danareksa.investasik.ui.activities.ListOfCatalogueActivity;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by fajarfatur on 2/2/16.
 */
public class PortfolioConfirmationPresenter {

    private PortfolioConfirmationFragment fragment;

    public PortfolioConfirmationPresenter(PortfolioConfirmationFragment fragment) {
        this.fragment = fragment;
    }

    void topupFee(final Packages packages) {
        fragment.showTopupLoading();
        fragment.getApi().subscriptionFee(packages.getId(), 1l, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Fee>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressDialog();
                        Timber.e(e.getLocalizedMessage());
                        fragment.hideTopupLoading();
                    }

                    @Override
                    public void onNext(List<Fee> list) {
                        fragment.hideTopupLoading();
                        if (list.size() > 0) {
                            fragment.topupFees = list;
                            fragment.setupTopupView();
                        }
                    }
                });

    }

    void topUpToCart(PortfolioInvestment investment, Packages packages, String ifuaId){
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().topUpToCart(Double.toString(packages.getMinTopupAmount()), investment.getInvestmentAccountNumber(), packages.getPackageCode(), "TOPUP", PrefHelper.getString(PrefKey.TOKEN), ifuaId)
        //fragment.getApi().topUpToCart(Double.toString(investment.getInvestmentAmount()), investment.getInvestmentAccountNumber(), packages.getPackageCode(), "TOPUP", PrefHelper.getString(PrefKey.TOKEN), ifuaId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted(){
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                        new MaterialDialog.Builder(fragment.getActivity())
                                .iconRes(R.mipmap.ic_launcher)
                                .backgroundColor(Color.WHITE)
                                .title(fragment.getString(R.string.failed).toUpperCase())
                                .titleColor(Color.BLACK)
                                .content(fragment.connectionError)
                                .contentColor(Color.GRAY)
                                .positiveText(R.string.ok)
                                .positiveColor(Color.GRAY)
                                .cancelable(false)
                                .show();
                    }

                    @Override
                    public void onNext(GenericResponse response) {
                        fragment.dismissProgressDialog();

                        if (response.getCode() == 1) {
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.failed).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(response.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .cancelable(false)
                                    .show();
                        }else if(response.getCode() == 50){
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.failed).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(response.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .cancelable(false)
                                    .show();
                        }else if(response.getCode() == 0){

                            ((BaseActivity) fragment.getActivity()).addNotifCount();

                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.success).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(R.string.catalogue_confirm_subscription)
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.yes)
                                    .positiveColor(Color.GRAY)
                                    .negativeText(R.string.catalogue_view_packages)
                                    .negativeColor(fragment.getResources().getColor(R.color.colorPrimary))
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            CartActivity.startActivity((BaseActivity) fragment.getActivity());
                                            fragment.getActivity().finish();
                                        }
                                    })
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            ListOfCatalogueActivity.startActivity((BaseActivity) fragment.getActivity());
                                            fragment.getActivity().finish();
                                        }
                                    })
                                    .cancelable(false)
                                    .show();
                        }else{
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.failed).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(response.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .cancelable(false)
                                    .show();
                        }

                    }
                });

    }

    public void cartList() {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().getCartList(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<CartListResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(List<CartListResponse> response) {
                        fragment.dismissProgressDialog();
                        ((BaseActivity) fragment.getActivity()).setNotifCount(0);

                        if (response != null && response.size() > 0) {
                            ((BaseActivity) fragment.getActivity()).setNotifCount(response.size());
                            fragment.cartList = response;

                            for (int i = 0; i < response.size(); i++) {
                                fundAllocation(response.get(i).getFundPackages().getId().longValue());
                            }

                            /*fragment.loadRule();*/
                        }
                    }
                });

    }

    void fundAllocation(Long id) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().fundAllocationInfo(id , PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<FundAllocationResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(FundAllocationResponse response) {
                        fragment.dismissProgressDialog();
                        fragment.response = response;
                    }
                });

    }


}
