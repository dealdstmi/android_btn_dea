package com.danareksa.investasik.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.fragments.kycnew.KycPersonalDataMain;
import com.danareksa.investasik.util.Constant;

import butterknife.Bind;

/**
 * Created by asep.surahman on 15/05/2018.
 */

public class RegisterDataPribadiActivity  extends BaseActivity{

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    LayoutInflater inflater;
    public static final String TAG = KycPersonalDataMain.class.getSimpleName();
    String sourceRoute;

    public static Activity registerActivity;

    public static void startActivity(BaseActivity sourceActivity, String sourceRoute){
        Intent intent = new Intent(sourceActivity, RegisterDataPribadiActivity.class);
        intent.putExtra(Constant.SOURCE_ROUTE, sourceRoute);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_register_data_pribadi;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerActivity = this;
        setupToolbar();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sourceRoute = getIntent().getStringExtra(Constant.SOURCE_ROUTE);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        KycPersonalDataMain.showFragment(this, sourceRoute);
    }


    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        //title.setText("Data Pribadi");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed(){
        System.out.println("===============>>>>here" + PrefHelper.getString(PrefKey.CUSTOMER_STATUS));
        /*super.onBackPressed();*/
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")){
            callConfirmationBackPage();
            System.out.println("====AA");
        }else{
            callConfirmation();
            System.out.println("====BB");
        }
    }

    public void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }

    private void callConfirmation(){
        KycPersonalDataMain fragment = (KycPersonalDataMain) getSupportFragmentManager().findFragmentByTag(TAG);
        fragment.showDialogKonfirmasi();
    }

    private void callConfirmationBackPage(){
        KycPersonalDataMain fragment = (KycPersonalDataMain) getSupportFragmentManager().findFragmentByTag(TAG);
        fragment.showDialogBackForm();
    }



}
