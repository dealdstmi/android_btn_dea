package com.danareksa.investasik.ui.fragments.changepassword;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by pandu.abbiyuarsyah on 14/03/2017.
 */

public class ChangePasswordFragment extends BaseFragment {

    public static final String TAG = ChangePasswordFragment.class.getSimpleName();

    @Bind(R.id.etConfirmPass)
    EditText etConfirmPass;
    @Bind(R.id.etOldPass)
    EditText etOldPass;
    @Bind(R.id.etNewPass)
    EditText etNewPass;
    @Bind(R.id.tvPwdHint)
    TextView tvPwdHint;
    @Bind(R.id.btSaveChangePass)
    Button btSaveChangePass;
    ChangePasswordPresenter presenter;


    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.container, new ChangePasswordFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.f_change_password;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ChangePasswordPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String hint = getString(R.string.signup_password_hint);
        tvPwdHint.setHint(Html.fromHtml(hint));
    }

    @OnClick(R.id.btSaveChangePass)
    void ChangePassword() {
        if(!validate()){
            onValidFailed();
            return;
        }else{
            presenter.requestChangePassword(presenter.constructChangePasswordRequest());
        }
    }


    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }



    public boolean validate(){

        boolean valid = true;
        String oldpass  = etOldPass.getText().toString();
        String newPass  = etNewPass.getText().toString();
        String conPass  = etConfirmPass.getText().toString();

        //String match = "^(?=.*[0-9])(?=.*?[.:;#?!@$%^&*-])(?=\S+$).{8,}$";
        //if(oldpass.isEmpty() || !oldpass.matches(match)){
        if(oldpass.isEmpty()){
            etOldPass.setError("Password lama tidak boleh kosong");
            valid = false;
        }

        String newpassmatch = "^(?=.*[0-9])(?=.*?[.:;#?!@$%^&*-])(?=\\S+$).{8,}$";
        if(newPass.isEmpty() || !newPass.matches(newpassmatch)){
            etNewPass.setError("Minimal 8 karakter terdiri dari kombinasi alphabet, angka dan spesial karakter");
            valid = false;
        }
        String conpassmatch = "^(?=.*[0-9])(?=.*?[.:;#?!@$%^&*-])(?=\\S+$).{8,}$";
        if(conPass.isEmpty() || !conPass.matches(conpassmatch)){
            etConfirmPass.setError("Minimal 8 karakter terdiri dari kombinasi alphabet, angka dan spesial karakter");
            valid = false;
        }

        if(!conPass.equals(newPass)){
            etConfirmPass.setError("Confirm password tidak sesuai dengan password baru");
            valid = false;
        }

        return valid;
    }

}
