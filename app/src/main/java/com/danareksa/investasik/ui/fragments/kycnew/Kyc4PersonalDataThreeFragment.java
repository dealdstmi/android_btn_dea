package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.City;
import com.danareksa.investasik.data.api.beans.Country;
import com.danareksa.investasik.data.api.beans.State;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 15/05/2018.
 */

public class Kyc4PersonalDataThreeFragment extends KycBaseNew{

    public static final String TAG = Kyc4PersonalDataThreeFragment.class.getSimpleName();
    protected Validator validator;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.sCountry)
    Spinner sCountry;
    @Bind(R.id.sState)
    Spinner sState;
    @Bind(R.id.sCity)
    Spinner sCity;
    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etAddressId)
    EditText etAddressId;
    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etKodePos)
    EditText etKodePos;
    @Bind(R.id.tvTryAgain)
    TextView tvTryAgain;
    public List<Country> countries;
    public List<State> states;
    public List<City> cities;
    public KycDataRequest kycDataRequest;
    private String jsonCountry;
    private String jsonState;
    private String jsonCity;
    String stateId = "";
    String countryId = "";

    Kyc4PersonalDataThreePresenter presenter;

    public static void showFragment(BaseActivity sourceActivity){
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc4PersonalDataThreeFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    public static Fragment getFragment(KycDataRequest kycDataRequest, String jsonCountry, String jsonState, String jsonCity) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        bundle.putString("jsonCountry", jsonCountry);
        bundle.putString("jsonState", jsonState);
        bundle.putString("jsonCity", jsonCity);
        Kyc4PersonalDataThreeFragment kyc4PersonalDataThreeFragment = new Kyc4PersonalDataThreeFragment();
        kyc4PersonalDataThreeFragment.setArguments(bundle);
        return kyc4PersonalDataThreeFragment;
    }

    @Override
    protected int getLayout(){
        return R.layout.f_register_data_pribadi_3;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        jsonCountry    = getArguments().getString("jsonCountry");
        jsonState      = getArguments().getString("jsonState");
        jsonCity       = getArguments().getString("jsonCity");
        kycDataRequest = (KycDataRequest) getArguments().getSerializable(KYC_DATA_REQUEST);
        presenter      = new Kyc4PersonalDataThreePresenter(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        init();
    }


    @Override
    public void onResume(){
        super.onResume();
    }


    public void init(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            disableField();
        }

        etKodePos.setText(request.getIdPostalCode());
        etAddressId.setText(request.getIdAddress());
        Type listType = new TypeToken<List<Country>>(){}.getType();
        Gson gson = new Gson();
        countries = gson.fromJson(jsonCountry, listType);

        Type listTypeState = new TypeToken<List<State>>() {}.getType();
        Gson gsonState = new Gson();
        states = gsonState.fromJson(jsonState, listTypeState);

        Type listTypeCity = new TypeToken<List<City>>() {}.getType();
        Gson gsonCity = new Gson();
        cities = gsonCity.fromJson(jsonCity, listTypeCity);

        if(request.getIdCountry() != null){
            setupSpinnerCountry(sCountry, countries, request.getIdCountry());
        }else{
            setupSpinnerCountry(sCountry, countries, Constant.ID_COUNTRY_INDO);
        }

        setupSpinnerState(sState, states, request.getIdProvince());
        setupSpinnerCity(sCity, cities, request.getIdCity());

        if(request.getIdCountry() != null){
            if(request.getIdCountry().equals(Constant.ID_COUNTRY_INDO)){

                stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());
                if(!stateId.equals("")){
                    presenter.getCity(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
                }else{
                    presenter.getCity(String.valueOf(3));
                }

            }else{
                if(!request.getIdCountry().equals("")){
                    presenter.getState(request.getIdCountry(), request.getIdProvince());
                }
            }
        }else{

            stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());
            if(!stateId.equals("")){
                presenter.getCity(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
            }else{
                presenter.getCity(String.valueOf(3));
            }
        }


        /*stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());
        if(!stateId.equals("")){
            System.out.println("state Id not null : " + stateId);
            presenter.getCity(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
        }else{
            System.out.println("state Id null : " + stateId);
            presenter.getCity(String.valueOf(3));
        }
        */

        onItemSelectedItem();
    }


    public void setValuePersonalData(){
        request.setIdPostalCode(getAndTrimValueFromEditText(etKodePos));
        request.setIdAddress(getAndTrimValueFromEditText(etAddressId));
        request.setIdCountry(String.valueOf(countries.get(sCountry.getSelectedItemPosition()).getId()));
        request.setIdProvince(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
        request.setIdCity(String.valueOf(cities.get(sCity.getSelectedItemPosition()).getId()));
        System.out.println("id country :  " + String.valueOf(countries.get(sCountry.getSelectedItemPosition()).getId()));
        System.out.println("Provvv : " + String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()) + " city : " + String.valueOf(cities.get(sCity.getSelectedItemPosition()).getId()));
    }



    @Override
    public void saveDataKycWithBackpress(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
                setValuePersonalData();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }

    @Override
    public void previewsWithoutValidation() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }



    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }else{
            if(!validate()){
                onValidFailed();
            }else{
                setValuePersonalData();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }
            return;
        }

    }


    //disable
    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
    }


    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    public void onItemSelectedItem(){
        sCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                countryId = String.valueOf(countries.get(sCountry.getSelectedItemPosition()).getId());

                if(sCountry.getSelectedItem().toString().equals("")){
                    countryId = "";
                    states = new ArrayList<>();
                    setupSpinnerState(sState, states, null);

                    cities = new ArrayList<>();
                    setupSpinnerCity(sCity, cities, null);
                }else{
                    if(!countryId.equals("")){
                        presenter.getState(countryId, "");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView){
            }
        });

        sState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());

                if (!stateId.equals("")) {
                    presenter.getCity(stateId);
                }else{
                    stateId = "";
                    cities = new ArrayList<>();
                    setupSpinnerCity(sCity, cities, null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }



    public boolean validate(){
        boolean valid = true;
        String addressId  = etAddressId.getText().toString();
        String postalKode = etKodePos.getText().toString();

        /*if(postalKode.isEmpty() && postalKode.length() <= 0){
            etKodePos.setError("Mohon isi kolom ini");
            valid = false;
        }*/

        if(addressId.isEmpty() && addressId.length() <= 0){
            etAddressId.setError("Mohon isi kolom ini");
            valid = false;
        }

        if(!addressId.isEmpty() && addressId.length() < 3){
            etAddressId.setError("Mohon isi alamat dengan benar");
            valid = false;
        }

        if(sCountry.getSelectedItem().toString().equals("") ||
            sCountry.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sCountry.getSelectedView()).setError("Mohon isi kolom ini");
        }

        if(sState.getSelectedItem().toString().equals("") ||
            sState.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sState.getSelectedView()).setError("Mohon isi kolom ini");
        }

        if(sCity.getSelectedItem().toString().equals("") ||
            sCity.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sCity.getSelectedView()).setError("Mohon isi kolom ini");
        }

        return valid;
    }



    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }


    public void disableField(){
        sCountry.setEnabled(false);
        sState.setEnabled(false);
        sCity.setEnabled(false);
        etAddressId.setEnabled(false);
        etKodePos.setEnabled(false);
    }


    @OnClick(R.id.tvTryAgain)
    void tvTryAgain(){
        stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());
        if(!stateId.equals("")){
            presenter.getCity(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
        }else{
            presenter.getCity(String.valueOf(3));
        }
    }




    @Override
    public void previewsWhileError(){

    }


}
