package com.danareksa.investasik.ui.fragments.promo;

import com.danareksa.investasik.data.api.responses.PromoListResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by pandu.abbiyuarsyah on 18/05/2017.
 */

public class PromoPresenter {

    private PromoFragment fragment;

    public PromoPresenter(PromoFragment fragment) {
        this.fragment = fragment;
    }

    void getPromoList() {
        fragment.showProgressBar();
        fragment.getApi().getPromoList(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PromoListResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(PromoListResponse promoListResponse) {

                        if (promoListResponse != null) {
                            fragment.listPromo = promoListResponse.getData();
                            fragment.getListPromo();
                            fragment.dismissProgressBar();
                            fragment.swipe_container.setRefreshing(false);
                        } else {
                            fragment.dismissProgressBar();
                            fragment.swipe_container.setRefreshing(false);
                        }
                        fragment.swipe_container.setRefreshing(false);
                    }
                });
    }


}
