package com.danareksa.investasik.ui.fragments.redemption;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.RedemptionData;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.MainActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.AmountFormatter;
import com.danareksa.investasik.util.DateUtil;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import icepick.State;

/**
 * Created by fajarfatur on 3/14/16.
 */
public class SummaryFragment extends BaseFragment {

    public static final String TAG = SummaryFragment.class.getSimpleName();
    private static final String REDEMPTION_DATA = "redemptionData";
    private static final String PACKAGES = "packages";
    private static final String PARTIALPERCENTAGE = "PartialPercentage";
    String percentage;

    @Bind(R.id.ivProduct)
    CircleImageView ivProduct;

    @Bind(R.id.txvOrderNumber)
    TextView txvOrderNumber;
    @Bind(R.id.txvInvestment)
    TextView txvInvestment;
    @Bind(R.id.txvPackageName)
    TextView txvPackageName;
    @Bind(R.id.txvMarketValue)
    TextView txvMarketValue;
    @Bind(R.id.txvFee)
    TextView txvFee;
    @Bind(R.id.txvRedeemAmount)
    TextView txvRedeemAmount;
    @Bind(R.id.txvAccountNumber)
    TextView txvAccountNumber;
    @Bind(R.id.txvAccountName)
    TextView txvAccountName;
    @Bind(R.id.txvBankName)
    TextView txvBankName;
    @Bind(R.id.txvExpirationTime)
    TextView txvExpirationTime;
    @Bind(R.id.txvPenjualanKembali)
    TextView txvPenjualanKembali;
    @Bind(R.id.tvNABDesc)
    TextView tvNABDesc;
    @Bind(R.id.wvNabDesc)
    WebView wvNabDesc;


/*    @Bind(R.id.txvDisclaimer1)
    TextView txvDisclaimer1;
    @Bind(R.id.txvDisclaimer2)
    TextView txvDisclaimer2;*/
 /*   @Bind(R.id.lnrData)
    LinearLayout lnrData;*/
   /* @Bind(R.id.llPartialPercentage)
    LinearLayout llPartialPercentage;*/
    /*@Bind(R.id.tvPartialPercenetage)
    TextView txvPartialPercenetage;*/


    @State
    RedemptionData redemptionData;
    @State
    Packages packages;

    Double tempPercentage;

    public static void showFragment(BaseActivity sourceActivity, RedemptionData redemptionData, Packages packages, String partialPercentage) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            SummaryFragment fragment = new SummaryFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(REDEMPTION_DATA, redemptionData);
            bundle.putSerializable(PACKAGES, packages);
            bundle.putString(PARTIALPERCENTAGE, partialPercentage);
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.f_redemption_summary;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        redemptionData = (RedemptionData) getArguments().getSerializable(REDEMPTION_DATA);
        packages = (Packages) getArguments().getSerializable(PACKAGES);
        percentage = getArguments().getString(PARTIALPERCENTAGE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tempPercentage = Double.parseDouble(percentage)*100;
        int valuePercentage =  tempPercentage.intValue();
        //txvPartialPercenetage.setText(valuePercentage+"%");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (redemptionData != null)
            bindingRedemptionData();
        /*if (!percentage.equals("0")){
            llPartialPercentage.setVisibility(View.VISIBLE);
        } else {
            llPartialPercentage.setVisibility(View.GONE);
        }*/
    }

    void bindingRedemptionData() {

        Picasso.with(getActivity()).load(InviseeService.IMAGE_DOWNLOAD_URL + packages.getPackageImage() + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                .placeholder(R.drawable.ic_logo_launcher)
                .error(R.drawable.ic_logo_launcher)
                .into(ivProduct);

        txvOrderNumber.setText(redemptionData.getOrderNumber());
        txvInvestment.setText(redemptionData.getIfua());
        txvPackageName.setText(redemptionData.getPackageName());
        txvMarketValue.setText(AmountFormatter.format(redemptionData.getMarketValue()));

        /*new*/
        double feeAmount  = redemptionData.getFeeAmount();// redemptionData.getRedeemAmount() * redemptionData.getFee();
        txvFee.setText(AmountFormatter.formatFeePercent(redemptionData.getFee()) + " | " + AmountFormatter.format(feeAmount));
        /*end*/

        //txvFee.setText(Double.toString(redemptionData.getFee()) + " %" );

        if(redemptionData.getProduct() != null && redemptionData.getProduct().size() > 0){
            txvRedeemAmount.setText(AmountFormatter.formatUnitWithComma4(redemptionData.getProduct().get(0).getOrderUnit())); //diganti unit terjual
        }

        txvPenjualanKembali.setText(AmountFormatter.format(redemptionData.getRedeemAmount()));
        txvAccountNumber.setText(redemptionData.getAccountNumber());
        txvAccountName.setText(redemptionData.getAccountName());
        txvBankName.setText(redemptionData.getBankName());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",    Locale.getDefault());
        //  Date date3 = sdf.parse("2017-02-03T11:44:52.6152Z");
        Date date3 = null;
        try {
            date3 = sdf.parse(redemptionData.getPaidDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String convertedDate= formatter.format(date3);

        String info = String.format(getString(R.string.redemption_expired_time), convertedDate);
        txvExpirationTime.setText(Html.fromHtml(info));

        Calendar transCutOff = Calendar.getInstance();
        transCutOff.setTime(DateUtil.format(packages.getTransactionCutOff(), DateUtil.INVISEE_RETURN_FORMAT2));


        date3 = null;
        try {
            date3 = sdf.parse(redemptionData.getProduct().get(0).getCustomerBalance().getBalanceDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        convertedDate = formatter.format(date3);

        String desc = "<html><head></head>\n" +
                " <body style='text-align:justify;color:#717073;font-size:14.3px'>Nilai penjualan kembali yang tercantum merupakan nilai indikasi berdasarkan NAB tanggal "+ convertedDate +". " +
                "Nilai penjualan kembali Anda bisa berubah sesuai dengan NAB pada saat pemrosesan transaksi Anda yaitu pada tanggal "+ redemptionData.getPriceDate()+".</body>\n" +
                "</html>";
        wvNabDesc.loadData(desc, "text/html; charset=utf-8", "utf-8");
    }


   /* public void loadList() {

        for (int i = 0; i < redemptionData.getProduct().size(); i++) {

            RedemptionProductComposition item = redemptionData.getProduct().get(i);

            LayoutInflater inflater = getActivity().getLayoutInflater();
            View v = inflater.inflate(R.layout.row_redemption_detail, null);

            TextView txvFundName = (TextView) v.findViewById(R.id.txvFundName);
            TextView txvFundAllocation = (TextView) v.findViewById(R.id.txvFundAllocation);

            txvFundName.setText(item.getUtProductName());
            txvFundAllocation.setText(Double.toString(item.getComposition()) + " %");

            lnrData.addView(v);

        }

    }
*/
    @OnClick(R.id.bOk)
    void close() {
        MainActivity.startActivity((BaseActivity) getActivity());
        getActivity().finish();
    }
}
