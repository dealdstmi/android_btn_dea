package com.danareksa.investasik.ui.fragments.redemption;


import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.OtpResult;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.danareksa.investasik.data.api.beans.RedemptionData;
import com.danareksa.investasik.data.api.beans.SendOtp;
import com.danareksa.investasik.data.api.responses.RedemptionOrderDetailResponse;
import com.danareksa.investasik.data.api.responses.SettlementInfoResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.RedemptionActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.github.irvingryan.VerifyCodeView;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.OnClick;
import icepick.State;

/*import com.tuenti.smsradar.Sms;
import com.tuenti.smsradar.SmsListener;
import com.tuenti.smsradar.SmsRadar;*/

/**
 * Created by fajarfatur on 3/14/16.
 */
public class AccountFragment extends BaseFragment {


    public static final String TAG = AccountFragment.class.getSimpleName();
    private static final String INVESTMENT = "investment";
    private static final String PACKAGES = "packages";
    private static final String STATUSREDEEM = "statusRedeem";
    public String percentageRedeem;
    private String statusRedeem="";

    @Bind(R.id.bSendPin)
    TextView bSendPin;
    @Bind(R.id.tvAccountName)
    TextView tvAccountName;
    @Bind(R.id.tvAccountNumber)
    TextView tvAccountNumber;
    @Bind(R.id.tvBranch)
    TextView tvBranch;
    @Bind(R.id.etOtp)
    VerifyCodeView etOtp;
    @Bind(R.id.tvCountDown)
    TextView tvCountDown;

    @Bind(R.id.txvSecurityPin)
    TextView txvSecurityPin;
    @BindString(R.string.redemption_please_input)
    String inputDigit;
    @State
    SettlementInfoResponse settlementInfoResponse;
    @State
    PortfolioInvestment investment;
    @State
    Packages packages;
    @State
    RedemptionOrderDetailResponse redemptionOrderDetailResponse;

    private AccountPresenter presenter;

    //CountDownTimer countDownTimer;

    OtpResult otpResult;
    SendOtp sendOtp;

    public static void showFragment(BaseActivity sourceActivity, PortfolioInvestment investment, Packages packages, String statusRedem) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            AccountFragment fragment = new AccountFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(INVESTMENT, investment);
            bundle.putSerializable(PACKAGES, packages);
            bundle.putString(STATUSREDEEM, statusRedem);
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayout() {
        return R.layout.f_redemption_account_new;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        otpResult = new OtpResult();
        sendOtp = new SendOtp();

        presenter = new AccountPresenter(this);
        if (investment == null )
            investment = (PortfolioInvestment) getArguments().getSerializable(INVESTMENT);
        if (packages == null )
            packages = (Packages) getArguments().getSerializable(PACKAGES);
        if (statusRedeem.equals("")){
            statusRedeem = getArguments().getString(STATUSREDEEM);
            Log.d(TAG, "onCreate: "+ statusRedeem);
        }
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (settlementInfoResponse != null) {
            bindingSettlementInfoData();
        } else {
            presenter.loadSettlementInfo();
        }

    }

    void bindingSettlementInfoData(){
        tvAccountName.setText(settlementInfoResponse.getSettlementAccountName());
        tvAccountNumber.setText(settlementInfoResponse.getSettlementAccountNo());
        tvBranch.setText(settlementInfoResponse.getBankName());
    }

    @OnClick(R.id.bProceed)
    void proceed(){

        if(!etOtp.getText().toString().equals("")){
            presenter.sendOtp(etOtp.getText().toString(), otpResult.getTrx_id());
        }else{
            Toast.makeText(getActivity(), "Masukkan OTP", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
    }


    public void getRedeemtion(){
        if(statusRedeem.equals("full")){
            presenter.createRedemption(presenter.constructCreateRedemptionRequest());
        } else {
            percentageRedeem = statusRedeem;
            presenter.createPartialRedemption(presenter.constructCreatePartialRedemptionRequest());
        }
    }


    public void gotoSummary(RedemptionData redemptionData, String percentage){
        ((RedemptionActivity) getActivity()).setStep(3);
        SummaryFragment.showFragment((BaseActivity) getActivity(), redemptionData, packages, percentage);
        //SmsRadar.stopSmsRadarService(getActivity()); //stop sms radar
        //PortfolioActivity.portoActivity.finish();
    }

    @OnClick(R.id.bSendPin)
    public void generateNewPin(){
        tvCountDown.setVisibility(View.VISIBLE);
        bSendPin.setVisibility(View.GONE);
        presenter.requestOtp();
    }


    public void succesRequestOtp(){
        reverseTimer(60, tvCountDown);

        /*SmsRadar.initializeSmsRadarService(getActivity(), new SmsListener(){
            @Override
            public void onSmsSent(Sms sms) {
                System.out.println("sms : " + sms);
            }

            @Override
            public void onSmsReceived(Sms sms){
                System.out.println("sms : " + sms);
                String [] data = sms.getMsg().split(" ");

                System.out.println("data : " + data[0] + " " + data[1]+ " " + data[2] + " " + data[3] + " " + data[4] + " " + data[5]);
                //etOtp.setText(data[3]);
                Log.e("dataSms",""+sms.getMsg());

                countDownTimer.cancel();
            }
        });*/
    }


    public void reverseTimer(int Seconds, final TextView tv){

        //countDownTimer = new CountDownTimer(Seconds * 1000, 1000){
        new CountDownTimer(Seconds * 1000, 1000){

            public void onTick(long millisUntilFinished){
                tv.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish(){
                //tv.setText("Completed");
                bSendPin.setVisibility(View.VISIBLE);
                tvCountDown.setVisibility(View.GONE);
            }
        }.start();
    }


    /*@Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(this.mIntentReceiver);
    }
*/



}
