package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RedemptionFee implements Serializable {

    @SerializedName("feeAmount")
    @Expose
    private Double feeAmount;
    @SerializedName("dayMin")
    @Expose
    private Double dayMin;
    @SerializedName("dayMax")
    @Expose
    private Double dayMax;

    public Double getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(Double feeAmount) {
        this.feeAmount = feeAmount;
    }

    public Double getDayMin() {
        return dayMin;
    }

    public void setDayMin(Double dayMin) {
        this.dayMin = dayMin;
    }

    public Double getDayMax() {
        return dayMax;
    }

    public void setDayMax(Double dayMax) {
        this.dayMax = dayMax;
    }

}
