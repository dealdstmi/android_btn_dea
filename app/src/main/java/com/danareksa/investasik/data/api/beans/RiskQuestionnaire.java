package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 19/07/2018.
 */

public class RiskQuestionnaire {

    @SerializedName("riskQuestionId")
    @Expose
    private String riskQuestionId;

    @SerializedName("riskAnswerId")
    @Expose
    private String riskAnswerId;


    public String getRiskQuestionId() {
        return riskQuestionId;
    }

    public void setRiskQuestionId(String riskQuestionId) {
        this.riskQuestionId = riskQuestionId;
    }

    public String getRiskAnswerId() {
        return riskAnswerId;
    }

    public void setRiskAnswerId(String riskAnswerId) {
        this.riskAnswerId = riskAnswerId;
    }
}
