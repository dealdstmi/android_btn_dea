package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by asep.surahman on 25/07/2018.
 */

public class PackageListReguler {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("subscriptionFee")
    @Expose
    private List<SubscriptionFee> subscriptionFeeList;

    @SerializedName("minSubscription")
    @Expose
    private double minSubscription;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("prospectusKey")
    @Expose
    private String prospectusKey;

    @SerializedName("code")
    @Expose
    private String code;

    //new
    @SerializedName("specialFee")
    @Expose
    private boolean specialFee;

    @SerializedName("pcgSpecialFee")
    @Expose
    private Double pcgSpecialFee;
    //end

    private String transactionAmount = "0";
    private String feePercentage = "0";
    private Double feePrice = 0d;
    private String total = "0";
    private String timePeriod;
    private boolean accept = false;
    private boolean isFirst = false;


    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getFeePercentage() {
        return feePercentage;
    }

    public void setFeePercentage(String feePercentage) {
        this.feePercentage = feePercentage;
    }

    public Double getFeePrice() {
        return feePrice;
    }

    public void setFeePrice(Double feePrice) {
        this.feePrice = feePrice;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<SubscriptionFee> getSubscriptionFeeList() {
        return subscriptionFeeList;
    }

    public void setSubscriptionFeeList(List<SubscriptionFee> subscriptionFeeList) {
        this.subscriptionFeeList = subscriptionFeeList;
    }

    public double getMinSubscription() {
        return minSubscription;
    }

    public void setMinSubscription(double minSubscription) {
        this.minSubscription = minSubscription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProspectusKey() {
        return prospectusKey;
    }

    public void setProspectusKey(String prospectusKey) {
        this.prospectusKey = prospectusKey;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public boolean isAccept() {
        return accept;
    }

    public void setAccept(boolean accept) {
        this.accept = accept;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public boolean isSpecialFee() {
        return specialFee;
    }

    public void setSpecialFee(boolean specialFee) {
        this.specialFee = specialFee;
    }

    public Double getPcgSpecialFee() {
        return pcgSpecialFee;
    }

    public void setPcgSpecialFee(Double pcgSpecialFee) {
        this.pcgSpecialFee = pcgSpecialFee;
    }
}
