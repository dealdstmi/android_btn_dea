package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.TrxTransCode;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by asep.surahman on 12/08/2018.
 */

public class TrxTransCodeResponse extends GenericResponse implements Serializable{

    @SerializedName("data")
    @Expose
    private List<TrxTransCode> data;

    public List<TrxTransCode> getData() {
        return data;
    }

    public void setData(List<TrxTransCode> data) {
        this.data = data;
    }
}
