package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.TermAndCondition;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rizal Gunawan on 7/18/18.
 */

public class TermAndConditionResponse implements Serializable {

    @SerializedName("code")
    private Integer code;

    @SerializedName("data")
    private TermAndCondition data;

    /**
     * @return The code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * @return The data
     */
    public TermAndCondition getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(TermAndCondition data) {
        this.data = data;
    }
}
