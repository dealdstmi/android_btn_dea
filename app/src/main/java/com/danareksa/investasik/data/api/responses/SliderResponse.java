package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Slider;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pandu.abbiyuarsyah on 15/11/2017.
 */

public class SliderResponse extends GenericResponse implements Serializable{

    @SerializedName("data")
    @Expose
    private List<Slider> data = new ArrayList<>();

    public List<Slider> getData() {
        return data;
    }

    public void setData(List<Slider> data) {
        this.data = data;
    }
}
