package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 25/07/2018.
 */

public class CustomerDataRekeningLumpsum {

    @SerializedName("nextAutoDebit")
    @Expose
    private String nextAutoDebit;

    @SerializedName("cif")
    @Expose
    private String cif;

    @SerializedName("fullname")
    @Expose
    private String fullname;

    @SerializedName("idNumber")
    @Expose
    private String idNumber;

    @SerializedName("bankAccount")
    @Expose
    private BankAccount bankAccount;

    public String getNextAutoDebit() {
        return nextAutoDebit;
    }

    public void setNextAutoDebit(String nextAutoDebit) {
        this.nextAutoDebit = nextAutoDebit;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }
}
