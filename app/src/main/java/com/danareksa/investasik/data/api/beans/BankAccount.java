package com.danareksa.investasik.data.api.beans;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 25/07/2018.
 */

public class BankAccount {

    @SerializedName("bankAccountNumber")
    @Expose
    private String bankAccountNumber;

    @SerializedName("bankName")
    @Expose
    private String bankName;

    @SerializedName("bankAccountId")
    @Expose
    private int bankAccountId;

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public int getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(int bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    @Override
    public String toString(){
        if(!getBankName().equals("") && !getBankAccountNumber().equals("")){
            return getBankName() + "-" + getBankAccountNumber();
        }else{
            return getBankName();
        }

    }


}
