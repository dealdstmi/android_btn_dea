package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.SendOtp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 23/08/2018.
 */

public class SendOtpResponse extends GenericResponse {

    @SerializedName("data")
    @Expose
    private SendOtp data;

    public SendOtp getData() {
        return data;
    }

    public void setData(SendOtp data) {
        this.data = data;
    }
}
