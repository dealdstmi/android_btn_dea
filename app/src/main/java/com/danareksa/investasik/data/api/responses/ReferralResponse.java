package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Referral;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pandu.abbiyuarsyah on 12/02/2018.
 */

public class ReferralResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private Referral data;



    public Referral getData() {
        return data;
    }

    public void setData(Referral data) {
        this.data = data;
    }

}
