package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 03/08/2018.
 */

public class PackageList implements Serializable {

    @SerializedName("packageId")
    @Expose
    private String packageId;

    @SerializedName("regulerPeriod")
    @Expose
    private String regulerPeriod;

    @SerializedName("monthlyInvestmentAmount")
    @Expose
    private String monthlyInvestmentAmount;

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getRegulerPeriod() {
        return regulerPeriod;
    }

    public void setRegulerPeriod(String regulerPeriod) {
        this.regulerPeriod = regulerPeriod;
    }

    public String getMonthlyInvestmentAmount() {
        return monthlyInvestmentAmount;
    }

    public void setMonthlyInvestmentAmount(String monthlyInvestmentAmount) {
        this.monthlyInvestmentAmount = monthlyInvestmentAmount;
    }
}
