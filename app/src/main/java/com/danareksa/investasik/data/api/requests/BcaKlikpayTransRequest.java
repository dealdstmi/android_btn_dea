package com.danareksa.investasik.data.api.requests;

import com.danareksa.investasik.data.api.beans.DetailBcaKlikpay;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 28/09/2018.
 */

public class BcaKlikpayTransRequest  {

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("appsType")
    @Expose
    private String appsType;

    @SerializedName("paymentMethod")
    @Expose
    private String paymentMethod;

    @SerializedName("detail")
    @Expose
    private DetailBcaKlikpay detail;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAppsType() {
        return appsType;
    }

    public void setAppsType(String appsType) {
        this.appsType = appsType;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public DetailBcaKlikpay getDetail() {
        return detail;
    }

    public void setDetail(DetailBcaKlikpay detail) {
        this.detail = detail;
    }
}
