package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Document;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pandu.abbiyuarsyah on 27/02/2017.
 */

public class CustomerDocumentResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private Document data;

    /**
     * @return The data
     */
    public Document getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(Document data) {
        this.data = data;
    }

}
