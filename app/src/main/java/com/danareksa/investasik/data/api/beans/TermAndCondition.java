package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rizal Gunawan on 7/18/18.
 */

public class TermAndCondition {

    @SerializedName("terms")
    @Expose
    private String terms;

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }
}
