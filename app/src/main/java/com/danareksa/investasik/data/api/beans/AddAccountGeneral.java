package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by asep.surahman on 08/08/2018.
 */

public class AddAccountGeneral implements Serializable{


    @SerializedName("goalDetail")
    @Expose
    private String goalDetail;

    @SerializedName("accountTypeCode")
    @Expose
    private String accountTypeCode;

    @SerializedName("goalCode")
    @Expose
    private String goalCode;

    @SerializedName("apssType")
    @Expose
    private String apssType;

    @SerializedName("bankAccountId")
    @Expose
    private Integer bankAccountId;

    @SerializedName("bankId")
    @Expose
    private Integer bankId;

    @SerializedName("bankBranchId")
    @Expose
    private Integer bankBranchId;

    @SerializedName("countryId")
    @Expose
    private Integer countryId;

    @SerializedName("bankAccountName")
    @Expose
    private String bankAccountName;

    @SerializedName("bankAccountNumber")
    @Expose
    private String bankAccountNumber;

    @SerializedName("bankAccountImage")
    @Expose
    private BankAccountImage bankAccountImage;

    @SerializedName("heirList")
    @Expose
    private List<HeirList> heirList;

    @SerializedName("packageList")
    @Expose
    private List<PackageList> packageList;

    public String getAccountTypeCode() {
        return accountTypeCode;
    }

    public void setAccountTypeCode(String accountTypeCode) {
        this.accountTypeCode = accountTypeCode;
    }

    public String getGoalCode() {
        return goalCode;
    }

    public void setGoalCode(String goalCode) {
        this.goalCode = goalCode;
    }

    public String getApssType() {
        return apssType;
    }

    public void setApssType(String apssType) {
        this.apssType = apssType;
    }

    public Integer getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Integer bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public Integer getBankBranchId() {
        return bankBranchId;
    }

    public void setBankBranchId(Integer bankBranchId) {
        this.bankBranchId = bankBranchId;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public BankAccountImage getBankAccountImage() {
        return bankAccountImage;
    }

    public void setBankAccountImage(BankAccountImage bankAccountImage) {
        this.bankAccountImage = bankAccountImage;
    }

    public List<HeirList> getHeirList() {
        return heirList;
    }

    public void setHeirList(List<HeirList> heirList) {
        this.heirList = heirList;
    }

    public List<PackageList> getPackageList() {
        return packageList;
    }

    public void setPackageList(List<PackageList> packageList) {
        this.packageList = packageList;
    }

    public String getGoalDetail() {
        return goalDetail;
    }

    public void setGoalDetail(String goalDetail) {
        this.goalDetail = goalDetail;
    }
}
