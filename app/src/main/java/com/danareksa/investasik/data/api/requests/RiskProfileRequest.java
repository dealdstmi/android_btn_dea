package com.danareksa.investasik.data.api.requests;

import com.danareksa.investasik.data.api.beans.RiskProfileNew;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by asep.surahman on 15/07/2018.
 */

public class RiskProfileRequest implements Serializable{

    @SerializedName("risk_profile")
    @Expose
    private List<RiskProfileNew> risk_profile;


    public List<RiskProfileNew> getRisk_profile(){
        return risk_profile;
    }

    public void setRisk_profile(List<RiskProfileNew> risk_profile){
        this.risk_profile = risk_profile;
    }
}
