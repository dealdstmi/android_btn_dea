package com.danareksa.investasik.data.api.requests;

import com.danareksa.investasik.data.api.beans.AddAccountGeneral;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 08/08/2018.
 */

public class AddAccountRequest extends AddAccountGeneral implements Serializable {

    @SerializedName("token")
    @Expose
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
