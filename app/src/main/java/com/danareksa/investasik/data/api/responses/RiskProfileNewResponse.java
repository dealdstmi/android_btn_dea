package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.RiskProfileData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 15/07/2018.
 */

public class RiskProfileNewResponse extends GenericResponse {


    @SerializedName("data")
    @Expose
    private RiskProfileData data;

    public RiskProfileData getData() {
        return data;
    }

    public void setData(RiskProfileData data) {
        this.data = data;
    }
}
