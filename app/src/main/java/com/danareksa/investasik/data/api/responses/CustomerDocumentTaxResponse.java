package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.DocumentTax;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pandu.abbiyuarsyah on 27/02/2017.
 */

public class CustomerDocumentTaxResponse {

    @SerializedName("data")
    @Expose
    private DocumentTax data;

    /**
     * @return The data
     */
    public DocumentTax getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(DocumentTax data) {
        this.data = data;
    }

}
