package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 31/10/2018.
 */

public class ValidateIfuaReguler {

    @SerializedName("special_fee")
    @Expose
    private boolean special_fee;

    @SerializedName("product_code")
    @Expose
    private String product_code;

    @SerializedName("fee_pcg")
    @Expose
    private Double fee_pcg;

    @SerializedName("ifua")
    @Expose
    private String ifua;

}
