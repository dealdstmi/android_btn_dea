package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Kyc;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by fajarfatur on 2/22/16.
 */
public class LoadKycDataResponse extends com.danareksa.investasik.data.api.responses.GenericResponse implements Serializable{

    @SerializedName("data")
    @Expose
    private Kyc data;

    /**
     * @return The data
     */
    public Kyc getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(Kyc data) {
        this.data = data;
    }

}
