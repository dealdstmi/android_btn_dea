package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 28/09/2018.
 */

public class RequestDataBcaKlikPay implements Serializable{

    @SerializedName("channelId")
    @Expose
    private String channelId;

    @SerializedName("serviceCode")
    @Expose
    private String serviceCode;

    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("transactionNo")
    @Expose
    private String transactionNo;

    @SerializedName("transactionAmount")
    @Expose
    private String transactionAmount;

    @SerializedName("transactionFee")
    @Expose
    private String transactionFee;

    @SerializedName("transactionDate")
    @Expose
    private String transactionDate;

    @SerializedName("transactionExpire")
    @Expose
    private String transactionExpire;

    @SerializedName("callbackUrl")
    @Expose
    private String callbackUrl;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("authCode")
    @Expose
    private String authCode;


    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionFee() {
        return transactionFee;
    }

    public void setTransactionFee(String transactionFee) {
        this.transactionFee = transactionFee;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionExpire() {
        return transactionExpire;
    }

    public void setTransactionExpire(String transactionExpire) {
        this.transactionExpire = transactionExpire;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }
}
