package com.danareksa.investasik.data.realm.model;

import io.realm.RealmObject;

/**
 * Created by fajarfatur on 2/18/16.
 */
public class KycLookupModel extends RealmObject {

    private String  category;
    private String  code;
    private String  value;
    private int seq;

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
