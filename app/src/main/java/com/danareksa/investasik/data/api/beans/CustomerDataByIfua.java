package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 19/09/2018.
 */

public class CustomerDataByIfua implements Serializable {

    @SerializedName("invAccGroupType")
    @Expose
    private String invAccGroupType;

    @SerializedName("settlementBankAccName")
    @Expose
    private String settlementBankAccName;

    @SerializedName("settlementBankAccNumber")
    @Expose
    private String settlementBankAccNumber;

    @SerializedName("nextAutoDebit")
    @Expose
    private String nextAutoDebit;

    @SerializedName("ifuaNumber")
    @Expose
    private String ifuaNumber;

    @SerializedName("settlementBankName")
    @Expose
    private String settlementBankName;

    public String getInvAccGroupType() {
        return invAccGroupType;
    }

    public void setInvAccGroupType(String invAccGroupType) {
        this.invAccGroupType = invAccGroupType;
    }

    public String getSettlementBankAccName() {
        return settlementBankAccName;
    }

    public void setSettlementBankAccName(String settlementBankAccName) {
        this.settlementBankAccName = settlementBankAccName;
    }

    public String getSettlementBankAccNumber() {
        return settlementBankAccNumber;
    }

    public void setSettlementBankAccNumber(String settlementBankAccNumber) {
        this.settlementBankAccNumber = settlementBankAccNumber;
    }

    public String getNextAutoDebit() {
        return nextAutoDebit;
    }

    public void setNextAutoDebit(String nextAutoDebit) {
        this.nextAutoDebit = nextAutoDebit;
    }

    public String getIfuaNumber() {
        return ifuaNumber;
    }

    public void setIfuaNumber(String ifuaNumber) {
        this.ifuaNumber = ifuaNumber;
    }

    public String getSettlementBankName() {
        return settlementBankName;
    }

    public void setSettlementBankName(String settlementBankName) {
        this.settlementBankName = settlementBankName;
    }
}
