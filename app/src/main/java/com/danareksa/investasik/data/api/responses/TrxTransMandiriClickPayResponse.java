package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.TrxTransCode;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Created by asep.surahman on 27/08/2018.
 */

public class TrxTransMandiriClickPayResponse extends GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private TrxTransCode data;

    public TrxTransCode getData() {
        return data;
    }

    public void setData(TrxTransCode data) {
        this.data = data;
    }
}
