package com.danareksa.investasik.data.api.requests;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 31/10/2018.
 */

public class ValidateSubscRegulerRequest {

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("ifuaNumber")
    @Expose
    private String ifuaNumber;

    @SerializedName("productCode")
    @Expose
    private String productCode;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIfuaNumber() {
        return ifuaNumber;
    }

    public void setIfuaNumber(String ifuaNumber) {
        this.ifuaNumber = ifuaNumber;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}
