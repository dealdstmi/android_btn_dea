package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.RedemptionData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fajarfatur on 3/16/16.
 */
public class CreateRedemptionResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private RedemptionData data;

    /**
     * @return The data
     */
    public RedemptionData getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(RedemptionData data) {
        this.data = data;
    }

}
