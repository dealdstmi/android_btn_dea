package com.danareksa.investasik.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PeriodeResp {
    @SerializedName("periode")
    @Expose
    private List<String> periode = null;

    public List<String> getPeriode() {
        return periode;
    }

    public void setPeriode(List<String> periode) {
        this.periode = periode;
    }
}
