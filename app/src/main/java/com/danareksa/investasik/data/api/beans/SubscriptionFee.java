package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 25/07/2018.
 */

public class SubscriptionFee {

    @SerializedName("amountMin")
    @Expose
    private Double amountMin;

    @SerializedName("amountMax")
    @Expose
    private Double amountMax;

    @SerializedName("feePercentage")
    @Expose
    private Double feePercentage;


    public Double getFeePercentage() {
        return feePercentage;
    }

    public void setFeePercentage(Double feePercentage) {
        this.feePercentage = feePercentage;
    }

    public Double getAmountMin() {
        return amountMin;
    }

    public void setAmountMin(Double amountMin) {
        this.amountMin = amountMin;
    }

    public Double getAmountMax() {
        return amountMax;
    }

    public void setAmountMax(Double amountMax) {
        this.amountMax = amountMax;
    }
}
