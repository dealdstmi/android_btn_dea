package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.CustomerDataRekeningLumpsum;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 25/07/2018.
 */

public class CustomerDataRekeningLumpsumResponse extends GenericResponse {


    @SerializedName("data")
    @Expose
    private CustomerDataRekeningLumpsum data;

    public CustomerDataRekeningLumpsum getData() {
        return data;
    }

    public void setData(CustomerDataRekeningLumpsum data) {
        this.data = data;
    }
}
