package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.SavaKyc;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 11/07/2018.
 */

public class SaveDataKycResponse extends GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private SavaKyc data;

    public SavaKyc getData() {
        return data;
    }

    public void setData(SavaKyc data) {
        this.data = data;
    }
}
