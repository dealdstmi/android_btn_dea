package com.danareksa.investasik.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fajarfatur on 3/16/16.
 */
public class CheckRedemptionResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("status")
    @Expose
    private boolean status;

    @SerializedName("statusSpecialFee")
    @Expose
    private boolean statusSpecialFee;

    @SerializedName("specialFee")
    @Expose
    private double specialFee;

    @SerializedName("totalDays")
    @Expose
    private Integer totalDays;

    /**
     * @return The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }


    public boolean isStatusSpecialFee() {
        return statusSpecialFee;
    }

    public void setStatusSpecialFee(boolean statusSpecialFee) {
        this.statusSpecialFee = statusSpecialFee;
    }

    public double getSpecialFee() {
        return specialFee;
    }

    public void setSpecialFee(double specialFee) {
        this.specialFee = specialFee;
    }

    public Integer getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(Integer totalDays) {
        this.totalDays = totalDays;
    }
}
