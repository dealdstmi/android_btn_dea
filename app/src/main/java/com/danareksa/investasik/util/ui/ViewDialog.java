package com.danareksa.investasik.util.ui;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.danareksa.investasik.R;

/**
 * Created by asep.surahman on 19/11/2018.
 */

public class ViewDialog {

    public void showDialog(Activity activity, String msg, String titleMsg){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_konfirm_one);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        TextView title = (TextView) dialog.findViewById(R.id.title_dialog);
        title.setText(titleMsg);
        
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

}
