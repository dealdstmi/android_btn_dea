package com.danareksa.investasik.util;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {

    public static byte[] EncryptByte(String value) {
        try {

            byte[] password = "Ini Adalah Data Kuncinya".getBytes("UTF-8");
            byte[] salt = { 0x00, 0x01, 0x02, 0x1C, 0x1D, 0x1E, 0x03, 0x04, 0x05, 0x0F, 0x20, 0x21, (byte) 0xAD,
                    (byte) 0xAF, (byte) 0xA4 };

            PasswordDeriveBytes pwd = new PasswordDeriveBytes(password, salt);

            SecretKeySpec keyspec = new SecretKeySpec(pwd.GetBytes(16), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(pwd.GetBytes(16));

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
            byte[] unicode = new String(value.getBytes(),"UTF-8").getBytes("UTF_16LE");
            byte[] encrypted = cipher.doFinal(unicode);

            return encrypted; //Base64.encodeToString(encrypted, Base64.NO_WRAP);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String Encrypt(String value) {
        try {

            byte[] password = "Ini Adalah Data Kuncinya".getBytes("UTF-8");
            byte[] salt = { 0x00, 0x01, 0x02, 0x1C, 0x1D, 0x1E, 0x03, 0x04, 0x05, 0x0F, 0x20, 0x21, (byte) 0xAD,
                    (byte) 0xAF, (byte) 0xA4 };

            PasswordDeriveBytes pwd = new PasswordDeriveBytes(password, salt);

            SecretKeySpec keyspec = new SecretKeySpec(pwd.GetBytes(16), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(pwd.GetBytes(16));

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
            byte[] unicode = new String(value.getBytes(),"UTF-8").getBytes("UTF_16LE");
            byte[] encrypted = cipher.doFinal(unicode);

            return Base64.encodeToString(encrypted, Base64.NO_WRAP);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    static class PasswordDeriveBytes {

        private String HashNameValue;
        private byte[] SaltValue;
        private int IterationsValue;

        private MessageDigest hash;
        private int state;
        private byte[] password;
        private byte[] initial;
        private byte[] output;
        private byte[] firstBaseOutput;
        private int position;
        private int hashnumber;
        private int skip;

        public PasswordDeriveBytes(String strPassword, byte[] rgbSalt) {
            Prepare(strPassword, rgbSalt, "SHA-1", 100);
        }

        public PasswordDeriveBytes(String strPassword, byte[] rgbSalt, String strHashName, int iterations) {
            Prepare(strPassword, rgbSalt, strHashName, iterations);
        }

        public PasswordDeriveBytes(byte[] password, byte[] salt) {
            Prepare(password, salt, "SHA-1", 100);
        }

        public PasswordDeriveBytes(byte[] password, byte[] salt, String hashName, int iterations) {
            Prepare(password, salt, hashName, iterations);
        }

        private void Prepare(String strPassword, byte[] rgbSalt, String strHashName, int iterations) {
            if (strPassword == null)
                throw new NullPointerException("strPassword");

            byte[] pwd = null;
            try {
                pwd = strPassword.getBytes("ASCII");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Prepare(pwd, rgbSalt, strHashName, iterations);
        }

        private void Prepare(byte[] password, byte[] rgbSalt, String strHashName, int iterations) {
            if (password == null)
                throw new NullPointerException("password");

            this.password = password;

            state = 0;
            setSalt(rgbSalt);
            setHashName(strHashName);
            setIterationCount(iterations);

            initial = new byte[hash.getDigestLength()];
        }

        public byte[] getSalt() {
            if (SaltValue == null)
                return null;
            return SaltValue;
        }

        public void setSalt(byte[] salt) {
            if (state != 0) {
                throw new SecurityException("Can't change this property at this stage");
            }
            if (salt != null)
                SaltValue = salt;
            else
                SaltValue = null;
        }

        public String getHashName() {
            return HashNameValue;
        }

        public void setHashName(String hashName) {
            if (hashName == null)
                throw new NullPointerException("HashName");
            if (state != 0) {
                throw new SecurityException("Can't change this property at this stage");
            }
            HashNameValue = hashName;

            try {
                hash = MessageDigest.getInstance(hashName);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        public int getIterationCount() {
            return IterationsValue;
        }

        public void setIterationCount(int iterationCount) {
            if (iterationCount < 1)
                throw new NullPointerException("HashName");
            if (state != 0) {
                throw new SecurityException("Can't change this property at this stage");
            }
            IterationsValue = iterationCount;
        }

        public byte[] GetBytes(int cb) throws DigestException {
            if (cb < 1) {
                throw new IndexOutOfBoundsException("cb");
            }

            if (state == 0) {
                Reset();
                state = 1;
            }

            byte[] result = new byte[cb];
            int cpos = 0;
            // the initial hash (in reset) + at least one iteration
            int iter = Math.max(1, IterationsValue - 1);

            // start with the PKCS5 key
            if (output == null) {
                // calculate the PKCS5 key
                output = initial;

                // generate new key material
                for (int i = 0; i < iter - 1; i++) {
                    output = hash.digest(output);
                }
            }

            while (cpos < cb) {
                byte[] output2 = null;
                if (hashnumber == 0) {
                    // last iteration on output
                    output2 = hash.digest(output);
                } else if (hashnumber < 1000) {
                    byte[] n = Integer.toString(hashnumber).getBytes();
                    output2 = new byte[output.length + n.length];
                    for (int j = 0; j < n.length; j++) {
                        output2[j] = n[j];
                    }
                    System.arraycopy(output, 0, output2, n.length, output.length);
                    // don't update output
                    output2 = hash.digest(output2);
                } else {
                    throw new SecurityException("too long");
                }

                int rem = output2.length - position;
                int l = Math.min(cb - cpos, rem);
                System.arraycopy(output2, position, result, cpos, l);

                cpos += l;
                position += l;
                while (position >= output2.length) {
                    position -= output2.length;
                    hashnumber++;
                }
            }

            // saving first output length
            if (state == 1) {
                if (cb > 20) {
                    skip = 40 - result.length;
                } else {
                    skip = 20 - result.length;
                }
                firstBaseOutput = new byte[result.length];
                System.arraycopy(result, 0, firstBaseOutput, 0, result.length);
                state = 2;
            }
            // processing second output
            else if (skip > 0) {
                byte[] secondBaseOutput = new byte[(firstBaseOutput.length + result.length)];
                System.arraycopy(firstBaseOutput, 0, secondBaseOutput, 0, firstBaseOutput.length);
                System.arraycopy(result, 0, secondBaseOutput, firstBaseOutput.length, result.length);
                System.arraycopy(secondBaseOutput, skip, result, 0, skip);

                skip = 0;
            }

            return result;
        }

        public void Reset() throws DigestException {
            state = 0;
            position = 0;
            hashnumber = 0;
            skip = 0;

            if (SaltValue != null) {
                hash.update(password, 0, password.length);
                hash.update(SaltValue, 0, SaltValue.length);
                hash.digest(initial, 0, initial.length);
            } else {
                initial = hash.digest(password);
            }
        }
    }

    private static String hexify(byte[] szBinary) {

        String szText = ""; // = new string[szBinary.Length*2+ 1];

        int ASCII_9 = Character.getNumericValue('9');// System.Convert.ToInt16('9');

        int ASCII_A = Character.getNumericValue('A');// System.Convert.ToInt16('A');

        int ASCII_0 = Character.getNumericValue('0');// System.Convert.ToInt16('0');

        String cHI, cLO;

        int iHI, iLO;

        for (int i = 0; i < szBinary.length; i++)

        {

            iHI = ((szBinary[i] >> 4) & 0xf) + ASCII_0;

            iLO = (szBinary[i] & 0xf) + ASCII_0;

            if (ASCII_9 < iHI)

                iHI += ASCII_A - ASCII_9 - 1;

            if (ASCII_9 < iLO)

                iLO += ASCII_A - ASCII_9 - 1;

            cHI = Integer.toHexString(iHI);// System.Convert.ToChar(iHI);

            cLO = Integer.toHexString(iLO);// System.Convert.ToChar(iLO);

            szText += cHI;

            szText += cLO;

        }

        return szText;

    }

    private static byte[] unHexify(String szText) throws Exception {

        if ((szText.length() % 2) > 0)
            throw new Exception("Invalid value");

        byte[] szBinary = new byte[szText.length() / 2];

        int ASCII_9 = Character.getNumericValue('9');// System.Convert.ToInt16('9');

        int ASCII_A = Character.getNumericValue('A');// System.Convert.ToInt16('A');

        int ASCII_0 = Character.getNumericValue('0');// System.Convert.ToInt16('0');

        char hi, lo;

        int iHI, iLO;

        char[] szTextArr = szText.toCharArray();

        for (int i = 0; i < szText.length(); i += 2)

        {

            hi = szTextArr[i];

            lo = szTextArr[i + 1];

            iHI = Character.getNumericValue(hi);

            iLO = Character.getNumericValue(lo);

            if (iHI > ASCII_9)

                iHI -= ASCII_A - ASCII_9 - 1;

            if (iLO > ASCII_9)

                iLO -= ASCII_A - ASCII_9 - 1;

            szBinary[i / 2] = (byte) (((iHI - ASCII_0) << 4) + (iLO - ASCII_0));

        }

        return szBinary;

    }
}
